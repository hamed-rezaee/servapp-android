package com.core.rezaee.servapp.profile;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.core.rezaee.servapp.BaseActivity;
import com.core.rezaee.servapp.Constant;
import com.core.rezaee.servapp.R;

import static com.core.rezaee.servapp.MainActivity.KEY_USER_ID;

public class ProfileActivity extends BaseActivity {

    private static final String TAG_CURRENT_FRAGMENT = "tag_current_fragment";

    private Fragment mCurrentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile);

        if (savedInstanceState == null) {

            String userId = Constant.EMPTY_STRING;

            if (getIntent().getExtras() != null) {
                userId = getIntent().getExtras().getString(KEY_USER_ID);
            }

            setFragment(userId);
        }

        initializeView();
        setupView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

        getSupportFragmentManager().putFragment(outState, TAG_CURRENT_FRAGMENT, mCurrentFragment);
    }

    public void onRestoreInstanceState(Bundle inState) {

        if (inState != null) {
            mCurrentFragment = getSupportFragmentManager().getFragment(inState, TAG_CURRENT_FRAGMENT);
        }
    }

    @Override
    public void initializeView() {
    }

    @Override
    public void setupView() {
    }

    public void setFragment(String userId) {

        mCurrentFragment = ProfileFragment.newInstance(userId);

        getSupportFragmentManager().beginTransaction().replace(R.id.activity_profile__view_container, mCurrentFragment, TAG_CURRENT_FRAGMENT).commit();
    }
}