package com.core.rezaee.servapp.user;

import com.core.rezaee.servapp.network.NetworkErrorCallback;

public interface UserCallback extends NetworkErrorCallback {

    void onUserRegistered(int requestCode, User user);

    void onTokenFetched(int requestCode, User user);

    void onActivationCodeFetched(int requestCode);

    void onPhoneNoValidated(int requestCode);

    void onUserFetched(int requestCode, User user);

    void onAccountInformationSent(int requestCode);
}