package com.core.rezaee.servapp.user;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.core.rezaee.servapp.Constant;
import com.core.rezaee.servapp.Helper;
import com.core.rezaee.servapp.LocalizeHelper;
import com.core.rezaee.servapp.R;
import com.core.rezaee.servapp.ServerError;

import timber.log.Timber;

import static com.core.rezaee.servapp.Constant.KEY_ENGLISH;
import static com.core.rezaee.servapp.Constant.KEY_PERSIAN;

public class RegisterFragment extends Fragment {

    private static final String LOGGER_TAG = "RegisterFragment";

    private static final int REQUEST_CODE_REGISTER_USER = 0;
    private static final int REQUEST_CODE_RECEIVE_SMS = 1;
    private static final int REQUEST_CODE_GET_ACTIVATION_CODE = 2;

    private EditText mUsernameEditText;
    private EditText mPhoneNoEditText;
    private EditText mPasswordEditText;
    private EditText mConfirmPasswordEditText;
    private EditText mPresenterCodeEditText;

    private Button mRegisterButton;
    private Button mLoginButton;
    private Button mActivationCodeTextView;

    private ImageView mLanguageImageView;

    private UserCallback userCallback = new UserCallbackWrapper() {

        @Override
        public void onUserRegistered(int requestCode, User user) {

            if (getActivity() == null) {
                return;
            }

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.RECEIVE_SMS}, REQUEST_CODE_RECEIVE_SMS);
            } else {
                getActivationCode();
            }

            Toast.makeText(getActivity(), R.string.fragment_register__user_registered_successfully, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onException(int requestCode, ServerError serverError) {

            Timber.e(serverError.message);

            if (requestCode == REQUEST_CODE_REGISTER_USER) {
                Toast.makeText(getActivity(), getString(serverError.code.getMessageResourceId()), Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register, container, false);

        initializeView(view);
        setupView();

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_CODE_RECEIVE_SMS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getActivationCode();
                }

                break;
        }
    }

    private void initializeView(View view) {

        mActivationCodeTextView = view.findViewById(R.id.fragment_register__text_view_activation_code);

        mUsernameEditText = view.findViewById(R.id.fragment_register__edit_text_username);
        mPhoneNoEditText = view.findViewById(R.id.fragment_register__edit_text_phone_no);
        mPasswordEditText = view.findViewById(R.id.fragment_register__edit_text_password);
        mConfirmPasswordEditText = view.findViewById(R.id.fragment_register__edit_text_confirm_password);
        mPresenterCodeEditText = view.findViewById(R.id.fragment_register__edit_text_presenter_code);

        mRegisterButton = view.findViewById(R.id.fragment_register__button_register);
        mLoginButton = view.findViewById(R.id.fragment_register__button_login);

        mLanguageImageView = view.findViewById(R.id.fragment_register__image_view_language);
    }

    private void setupView() {

        mRegisterButton.setEnabled(false);

        mUsernameEditText.addTextChangedListener(new CustomTextWatcher());
        mPhoneNoEditText.addTextChangedListener(new CustomTextWatcher());
        mPasswordEditText.addTextChangedListener(new CustomTextWatcher());
        mConfirmPasswordEditText.addTextChangedListener(new CustomTextWatcher());

        mPresenterCodeEditText.setOnEditorActionListener((view, actionId, event) -> {

            if (actionId == EditorInfo.IME_ACTION_DONE) {
                register();

                return true;
            } else {
                return false;
            }
        });

        mRegisterButton.setOnClickListener(view -> register());

        mLoginButton.setOnClickListener(view -> {

            if (getActivity() != null) {
                ((UserActivity) getActivity()).setFragment(UserInteractionMode.LOGIN);
            }
        });

        mActivationCodeTextView.setOnClickListener(view -> {

            if (getActivity() != null) {
                ((UserActivity) getActivity()).setFragment(UserInteractionMode.ACTIVATION_CODE);
            }
        });

        mLanguageImageView.setOnClickListener(view -> setLanguage());
    }

    private void register() {

        if (validateRequest()) {
            getRegistrationData();

            User.register(getActivity(), REQUEST_CODE_REGISTER_USER, userCallback, getRegistrationData());
        }
    }

    private void getActivationCode() {

        if (getActivity() == null) {
            return;
        }

        User.getActivationCode(getActivity(), REQUEST_CODE_GET_ACTIVATION_CODE, userCallback, getUserActivationCodeRequest());

        ((UserActivity) getActivity()).setFragment(UserInteractionMode.VERIFY, mPhoneNoEditText.getText().toString().trim());
    }

    private UserRegisterRequest getRegistrationData() {

        UserRegisterRequest request = new UserRegisterRequest();

        request.username = mUsernameEditText.getText().toString().trim();
        request.phoneNo = mPhoneNoEditText.getText().toString().trim();
        request.password = mPasswordEditText.getText().toString().trim();
        request.presenterCode = mPresenterCodeEditText.getText().toString().trim();

        return request;
    }

    private UserActivationCodeRequest getUserActivationCodeRequest() {

        return new UserActivationCodeRequest(mPhoneNoEditText.getText().toString().trim());
    }

    private boolean validateRequireFields() {

        return !(mUsernameEditText.getText().toString().trim().isEmpty() || mPasswordEditText.getText().toString().trim().isEmpty() || mConfirmPasswordEditText.getText().toString().trim().isEmpty());
    }

    private boolean validateRequest() {

        Helper.hideKeypad(getActivity());

        return validateUsername() && validatePhoneNo() && validatePassword();
    }

    private boolean validateUsername() {

        if (mUsernameEditText.getText().toString().trim().length() < Constant.MIN_TEXT_CHARACTER_LENGTH) {
            String message = String.format(getString(R.string.app_validation__min_char_length), getString(R.string.fragment_register__username), String.valueOf(Constant.MIN_TEXT_CHARACTER_LENGTH));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mUsernameEditText);

            return false;
        }

        if (!Helper.checkUsername(mUsernameEditText.getText().toString().trim())) {
            String message = String.format(getString(R.string.app_validation__wrong_format), getString(R.string.fragment_register__username));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mUsernameEditText);

            return false;
        }

        return true;
    }

    private boolean validatePhoneNo() {

        if (mPhoneNoEditText.getText().toString().trim().isEmpty()) {
            String message = String.format(getString(R.string.app_validation__required_field), getString(R.string.fragment_register__phone_no));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mPhoneNoEditText);

            return false;
        }

        if (mPhoneNoEditText.getText().toString().trim().length() < Constant.MIN_PHONE_NO_LENGTH) {
            String message = String.format(getString(R.string.app_validation__min_char_length), getString(R.string.fragment_register__phone_no), String.valueOf(Constant.MIN_PHONE_NO_LENGTH));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mPhoneNoEditText);

            return false;
        }

        if (!Helper.checkPhoneNo(mPhoneNoEditText.getText().toString().trim())) {
            String message = String.format(getString(R.string.app_validation__wrong_format), getString(R.string.fragment_register__phone_no));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mPhoneNoEditText);

            return false;
        }

        return true;
    }

    private boolean validatePassword() {

        if (mPasswordEditText.getText().toString().trim().length() < Constant.MIN_PASSWORD_CHARACTER_LENGTH) {
            String message = String.format(getString(R.string.app_validation__min_char_length), getString(R.string.fragment_register__password), String.valueOf(Constant.MIN_PASSWORD_CHARACTER_LENGTH));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mPasswordEditText);

            return false;
        }

        if (!Helper.checkPasswordComplexity(mPasswordEditText.getText().toString().trim())) {
            Toast.makeText(getActivity(), getString(R.string.app_validation__password_complexity), Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mPasswordEditText);

            return false;
        }

        if (!mPasswordEditText.getText().toString().trim().equals(mConfirmPasswordEditText.getText().toString().trim())) {
            Toast.makeText(getActivity(), getString(R.string.app_validation__password_confirm), Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mPasswordEditText);

            return false;
        }

        return true;
    }

    private void setLanguage() {

        if (getActivity() == null) {
            return;
        }

        LocalizeHelper.changeLocale(getActivity(), LocalizeHelper.isLanguageEnglish(getActivity()) ? KEY_PERSIAN : KEY_ENGLISH);
    }

    private class CustomTextWatcher implements TextWatcher {

        private CustomTextWatcher() {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {

            mRegisterButton.setEnabled(validateRequireFields());
        }
    }
}