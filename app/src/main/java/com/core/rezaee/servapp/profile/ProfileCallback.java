package com.core.rezaee.servapp.profile;

import com.core.rezaee.servapp.network.NetworkErrorCallback;

public interface ProfileCallback extends NetworkErrorCallback {

    void onProfileSaved(int requestCode);
}