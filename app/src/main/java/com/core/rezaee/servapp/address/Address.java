package com.core.rezaee.servapp.address;

import android.content.Context;
import android.support.annotation.NonNull;

import com.core.rezaee.servapp.Exchangeable;
import com.core.rezaee.servapp.GenericIdRequest;
import com.core.rezaee.servapp.Helper;
import com.core.rezaee.servapp.ServerError;
import com.core.rezaee.servapp.file.File;
import com.core.rezaee.servapp.models.AddressModel;
import com.core.rezaee.servapp.network.NetworkCallback;
import com.core.rezaee.servapp.network.NetworkHelper;
import com.core.rezaee.servapp.profile.Profile;
import com.core.rezaee.servapp.profile.ProfileAddressRequest;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class Address implements Serializable, Exchangeable<AddressModel> {

    private static final String LOGGER_TAG = "Address";

    public String id;
    public String title;
    public String detail;
    public String range;
    public double latitude;
    public double longitude;

    public Profile profile;
    public File staticMap;

    public Address() {
    }

    public Address(AddressModel model) {

        if (model == null) {
            return;
        }

        id = model._id;
        title = model.title;
        detail = model.detail;
        range = model.range;
        latitude = model.latitude;
        longitude = model.longitude;

        profile = new Profile(model.profile);
        staticMap = new File(model.staticMap);
    }

    public AddressModel getModel() {

        AddressModel model = new AddressModel();

        model._id = id;
        model.title = title;
        model.detail = detail;
        model.range = range;
        model.latitude = latitude;
        model.longitude = longitude;

        model.profile = profile.getModel();
        model.staticMap = staticMap.getModel();

        return model;
    }

    @Override
    public AddressModel exchange(Object... extraData) {

        return getModel();
    }

    public static void getProfileAddresses(Context context, final int requestCode, final AddressCallback callback, ProfileAddressRequest request) {

        AddressApi addressApi = NetworkHelper.getInstance(context).create(AddressApi.class);
        Call<List<AddressModel>> call = addressApi.getProfileAddresses(request.getModel());

        call.enqueue(new NetworkCallback<List<AddressModel>>(context, true) {

            @Override
            public void onResponse(@NonNull Call<List<AddressModel>> call, @NonNull Response<List<AddressModel>> response) {

                super.onResponse(call, response);

                if (!response.isSuccessful()) {
                    callback.onException(requestCode, ServerError.getError(response.errorBody()));
                } else {
                    callback.onAddressesFetched(requestCode, Helper.exchange(response.body()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<AddressModel>> call, @NonNull Throwable e) {

                super.onFailure(call, e);

                callback.onException(requestCode, ServerError.getError(null));
            }
        });
    }

    public static void save(Context context, final int requestCode, final AddressCallback callback, AddressSaveRequest request) {

        AddressApi addressApi = NetworkHelper.getInstance(context).create(AddressApi.class);
        Call<Void> call = addressApi.save(request.getModel());

        call.enqueue(new NetworkCallback<Void>(context, true) {

            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {

                super.onResponse(call, response);

                if (!response.isSuccessful()) {
                    callback.onException(requestCode, ServerError.getError(response.errorBody()));
                } else {
                    callback.onAddressesSaved(requestCode);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable e) {

                super.onFailure(call, e);

                callback.onException(requestCode, ServerError.getError(null));
            }
        });
    }

    public static void delete(Context context, final int requestCode, final AddressCallback callback, GenericIdRequest request) {

        AddressApi addressApi = NetworkHelper.getInstance(context).create(AddressApi.class);
        Call<Void> call = addressApi.delete(request.getModel());

        call.enqueue(new NetworkCallback<Void>(context, true) {

            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {

                super.onResponse(call, response);

                if (!response.isSuccessful()) {
                    callback.onException(requestCode, ServerError.getError(response.errorBody()));
                } else {
                    callback.onAddressesDeleted(requestCode);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable e) {

                super.onFailure(call, e);

                callback.onException(requestCode, ServerError.getError(null));
            }
        });
    }
}