package com.core.rezaee.servapp.profile;

import com.core.rezaee.servapp.models.requests.ProfileSaveRequestModel;

public class ProfileSaveRequest {

    public String id;
    public String firstName;
    public String lastName;
    public String nationalCode;
    public int score;
    public String userId;
    public String avatarId;

    public ProfileSaveRequestModel getModel() {

        ProfileSaveRequestModel model = new ProfileSaveRequestModel();

        model._id = id;
        model.firstName = firstName;
        model.lastName = lastName;
        model.nationalCode = nationalCode;
        model.score = score;
        model.userId = userId;
        model.avatarId = avatarId;

        return model;
    }
}