package com.core.rezaee.servapp.profile;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.core.rezaee.servapp.Constant;
import com.core.rezaee.servapp.GenericIdRequest;
import com.core.rezaee.servapp.Helper;
import com.core.rezaee.servapp.R;
import com.core.rezaee.servapp.ServerError;
import com.core.rezaee.servapp.file.File;
import com.core.rezaee.servapp.file.FileCallback;
import com.core.rezaee.servapp.file.FileCallbackWrapper;
import com.core.rezaee.servapp.file.FileHelper;
import com.core.rezaee.servapp.user.User;
import com.core.rezaee.servapp.user.UserCallback;
import com.core.rezaee.servapp.user.UserCallbackWrapper;
import com.github.abdularis.civ.CircleImageView;

import java.io.IOException;

import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.core.rezaee.servapp.MainActivity.KEY_USER_ID;

public class ProfileFragment extends Fragment {

    private static final String LOGGER_TAG = "ProfileFragment";

    private static final int REQUEST_CODE_GET_USER = 0;
    private static final int REQUEST_CODE_READ_FILE_PERMISSION = 1;
    private static final int REQUEST_CODE_FILE_SELECT = 2;
    private static final int REQUEST_CODE_UPLOAD_FILE = 3;
    private static final int REQUEST_CODE_SAVE_PROFILE = 4;

    private User mUser;
    private Uri mAvatarUri;

    private View mContainerView;

    private TextView mScoreTextView;

    private EditText mFirstNameEditText;
    private EditText mLastNameEditText;
    private EditText mNationalCodeEditText;

    private Button mOkButton;
    private Button mCancelButton;

    private CircleImageView mAvatarImageView;

    private ProgressBar mLoadingProgressBar;

    private UserCallback mUserCallback = new UserCallbackWrapper() {

        @Override
        public void onUserFetched(int requestCode, User user) {

            mUser = user;

            fillView(mUser);

            mContainerView.setVisibility(View.VISIBLE);
            mLoadingProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onException(int requestCode, ServerError serverError) {

            Timber.e(serverError.message);

            Toast.makeText(getActivity(), getString(serverError.code.getMessageResourceId()), Toast.LENGTH_SHORT).show();
        }
    };

    private ProfileCallback mProfileCallback = new ProfileCallbackWrapper() {

        @Override
        public void onProfileSaved(int requestCode) {

            if (getActivity() == null) {
                return;
            }

            Toast.makeText(getActivity(), R.string.app__changes_applied, Toast.LENGTH_SHORT).show();

            getActivity().finish();
        }

        @Override
        public void onException(int requestCode, ServerError serverError) {

            Timber.e(serverError.message);

            Toast.makeText(getActivity(), getString(serverError.code.getMessageResourceId()), Toast.LENGTH_SHORT).show();
        }
    };

    private FileCallback mFileCallback = new FileCallbackWrapper() {

        @Override
        public void onFileUploaded(int requestCode, File file) {

            Profile.save(getActivity(), REQUEST_CODE_SAVE_PROFILE, mProfileCallback, getRequest(file));
        }

        @Override
        public void onException(int requestCode, ServerError serverError) {

            Timber.e(serverError.message);

            if (requestCode == REQUEST_CODE_UPLOAD_FILE) {
                Toast.makeText(getActivity(), getString(serverError.code.getMessageResourceId()), Toast.LENGTH_SHORT).show();
            }
        }
    };


    public static ProfileFragment newInstance(String userId) {

        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();

        args.putSerializable(KEY_USER_ID, userId);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        initializeView(view);
        setupView();

        if (getArguments() != null) {
            User.getUser(getActivity(), REQUEST_CODE_GET_USER, mUserCallback, new GenericIdRequest(getArguments().getString(KEY_USER_ID)));
        }

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_CODE_READ_FILE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showFileChooser();
                }

                break;
        }
    }

    private void initializeView(View view) {

        mContainerView = view.findViewById(R.id.fragment_profile__view_container);

        mScoreTextView = view.findViewById(R.id.fragment_profile__text_view_score);

        mFirstNameEditText = view.findViewById(R.id.fragment_profile__edit_text_first_name);
        mLastNameEditText = view.findViewById(R.id.fragment_profile__edit_text_last_name);
        mNationalCodeEditText = view.findViewById(R.id.fragment_profile__edit_text_national_code);

        mOkButton = view.findViewById(R.id.fragment_profile__button_ok);
        mCancelButton = view.findViewById(R.id.fragment_profile__button_cancel);

        mAvatarImageView = view.findViewById(R.id.fragment_profile__image_view_avatar);

        mLoadingProgressBar = view.findViewById(R.id.fragment_profile__progress_bar_loading);
    }

    private void setupView() {

        mContainerView.setVisibility(View.GONE);
        mLoadingProgressBar.setVisibility(View.VISIBLE);

        mOkButton.setEnabled(false);

        mFirstNameEditText.addTextChangedListener(new CustomTextWatcher());
        mLastNameEditText.addTextChangedListener(new CustomTextWatcher());
        mNationalCodeEditText.addTextChangedListener(new CustomTextWatcher());

        mNationalCodeEditText.setOnEditorActionListener((view, actionId, event) -> {

            if (actionId == EditorInfo.IME_ACTION_DONE) {
                saveProfile();

                return true;
            } else {
                return false;
            }
        });

        mAvatarImageView.setOnClickListener(v -> {

            if (getActivity() == null) {
                return;
            }

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_READ_FILE_PERMISSION);
            } else {
                showFileChooser();
            }
        });

        mOkButton.setOnClickListener(view -> saveProfile());

        mCancelButton.setOnClickListener(view -> {

            if (getActivity() != null) {
                getActivity().finish();
            }
        });
    }

    private void fillView(User user) {

        mScoreTextView.setText(Helper.formatDecimalForDisplay(user.profile.score));

        mFirstNameEditText.setText(user.profile.firstName);
        mLastNameEditText.setText(user.profile.lastName);
        mNationalCodeEditText.setText(user.profile.nationalCode);

        if (user.profile != null && user.profile.avatar != null && user.profile.avatar.content != null) {
            Bitmap bitmap = Helper.getBitmap(user.profile.avatar.content.data);

            mAvatarImageView.setImageBitmap(bitmap);
        } else {
            if (getActivity() == null) {
                return;
            }

            mAvatarImageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_user));
        }
    }

    private void saveProfile() {

        if (validateRequest()) {
            if (mAvatarUri != null) {
                File avatar = mUser.profile.avatar;

                File.upload(getActivity(), REQUEST_CODE_UPLOAD_FILE, mFileCallback, (avatar == null || avatar.id == null) ? Constant.EMPTY_STRING : mUser.profile.avatar.id, mAvatarUri);
            } else {
                Profile.save(getActivity(), REQUEST_CODE_SAVE_PROFILE, mProfileCallback, getRequest(mUser.profile.avatar));
            }
        }
    }

    private ProfileSaveRequest getRequest(File file) {

        ProfileSaveRequest request = new ProfileSaveRequest();

        request.id = mUser.profile.id;
        request.firstName = mFirstNameEditText.getText().toString().trim();
        request.lastName = mLastNameEditText.getText().toString().trim();
        request.nationalCode = mNationalCodeEditText.getText().toString().trim();
        request.userId = mUser.id;
        request.avatarId = file == null ? null : file.id;

        return request;
    }

    private boolean validateRequireFields() {

        return !(mFirstNameEditText.getText().toString().trim().isEmpty() || mLastNameEditText.getText().toString().trim().isEmpty() || mNationalCodeEditText.getText().toString().trim().isEmpty());
    }

    private boolean validateRequest() {

        Helper.hideKeypad(getActivity());

        return true;
    }

    private void showFileChooser() {

        startActivityForResult(FileHelper.createGetContentIntent(FileHelper.MIME_TYPE_IMAGE), REQUEST_CODE_FILE_SELECT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (getActivity() == null) {
            return;
        }

        switch (requestCode) {
            case REQUEST_CODE_FILE_SELECT:
                if (resultCode == RESULT_OK) {
                    mAvatarUri = data.getData();

                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), mAvatarUri);

                        mAvatarImageView.setImageBitmap(bitmap);
                    } catch (IOException ignored) {
                    }
                }

                break;
        }
    }

    private class CustomTextWatcher implements TextWatcher {

        private CustomTextWatcher() {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {

            mOkButton.setEnabled(validateRequireFields());
        }
    }
}