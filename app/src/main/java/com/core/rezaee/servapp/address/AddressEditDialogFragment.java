package com.core.rezaee.servapp.address;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.core.rezaee.servapp.Constant;
import com.core.rezaee.servapp.Helper;
import com.core.rezaee.servapp.R;
import com.core.rezaee.servapp.ServerError;
import com.core.rezaee.servapp.file.File;
import com.core.rezaee.servapp.file.FileCallback;
import com.core.rezaee.servapp.file.FileCallbackWrapper;

import timber.log.Timber;

import static com.core.rezaee.servapp.address.AddressAdapter.KEY_ADDRESS;
import static com.core.rezaee.servapp.address.AddressAdapter.KEY_STATIC_MAP;

public class AddressEditDialogFragment extends DialogFragment {

    private static final String LOGGER_TAG = "AddressEditDialogFragment";

    private static final int REQUEST_CODE_SAVE_ADDRESS = 0;
    private static final int REQUEST_CODE_UPLOAD_FILE = 1;

    private Address mAddress;
    private Bitmap mStaticMap;

    private TextView mRangeTextView;
    private EditText mTitleEditText;
    private EditText mDetailEditText;
    private Button mOkButton;
    private Button mCancelButton;

    private AddressCallback mAddressCallback = new AddressCallbackWrapper() {

        @Override
        public void onAddressesSaved(int requestCode) {

            if (getTargetFragment() == null) {
                return;
            }

            dismiss();

            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
        }

        @Override
        public void onException(int requestCode, ServerError serverError) {

            Timber.e(serverError.message);

            Toast.makeText(getActivity(), getString(serverError.code.getMessageResourceId()), Toast.LENGTH_SHORT).show();
        }
    };

    private FileCallback mFileCallback = new FileCallbackWrapper() {

        @Override
        public void onFileUploaded(int requestCode, File file) {

            if (getActivity() == null) {
                return;
            }

            Address.save(getActivity(), REQUEST_CODE_SAVE_ADDRESS, mAddressCallback, getSaveRequest(file.id));
        }

        @Override
        public void onException(int requestCode, ServerError serverError) {

            Timber.e(serverError.message);

            if (requestCode == REQUEST_CODE_UPLOAD_FILE) {
                Toast.makeText(getActivity(), getString(serverError.code.getMessageResourceId()), Toast.LENGTH_SHORT).show();
            }
        }
    };

    public static AddressEditDialogFragment newInstance(Address address, Bitmap staticMap) {

        AddressEditDialogFragment fragment = new AddressEditDialogFragment();
        Bundle args = new Bundle();

        args.putSerializable(KEY_ADDRESS, address);
        args.putParcelable(KEY_STATIC_MAP, staticMap);

        fragment.setArguments(args);

        return fragment;
    }

    public static AddressEditDialogFragment newInstance(Address address) {

        AddressEditDialogFragment fragment = new AddressEditDialogFragment();
        Bundle args = new Bundle();

        args.putSerializable(KEY_ADDRESS, address);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_address_edit, container, false);

        getDialog().setCancelable(false);

        initializeView(view);
        setupView();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            mAddress = (Address) getArguments().getSerializable(KEY_ADDRESS);
            mStaticMap = getArguments().getParcelable(KEY_STATIC_MAP);
        }

        fillView();
    }

    private void initializeView(View view) {

        mRangeTextView = view.findViewById(R.id.fragment_address_edit__text_view_range);

        mTitleEditText = view.findViewById(R.id.fragment_address_edit__edit_text_title);
        mDetailEditText = view.findViewById(R.id.fragment_address_edit__edit_text_detail);

        mOkButton = view.findViewById(R.id.fragment_address_edit__button_ok);
        mCancelButton = view.findViewById(R.id.fragment_address_edit__button_cancel);
    }

    private void setupView() {

        mTitleEditText.addTextChangedListener(new CustomTextWatcher());
        mDetailEditText.addTextChangedListener(new CustomTextWatcher());

        mOkButton.setEnabled(validateRequireFields());

        mOkButton.setOnClickListener(view -> {

            if (getActivity() == null) {
                return;
            }

            if (mAddress.staticMap != null) {
                Address.save(getActivity(), REQUEST_CODE_SAVE_ADDRESS, mAddressCallback, getSaveRequest(mAddress.staticMap.id));
            } else {
                File.upload(getActivity(), REQUEST_CODE_UPLOAD_FILE, mFileCallback, Constant.EMPTY_STRING, Helper.getStaticMapUri(getActivity(), mStaticMap));
            }
        });

        mCancelButton.setOnClickListener(view -> dismiss());
    }

    private void fillView() {

        if (getActivity() == null) {
            return;
        }

        mRangeTextView.setText(getActivity().getString(R.string.list_item_address__range, mAddress.range));

        mTitleEditText.setText(mAddress.title);
        mDetailEditText.setText(mAddress.detail);
    }

    private AddressSaveRequest getSaveRequest(String fileId) {

        AddressSaveRequest request = new AddressSaveRequest();

        request.id = mAddress.id;
        request.title = mTitleEditText.getText().toString().trim();
        request.detail = mDetailEditText.getText().toString().trim();
        request.range = mAddress.range;
        request.latitude = mAddress.latitude;
        request.longitude = mAddress.longitude;
        request.profileId = mAddress.profile.id;
        request.staticMapId = fileId;

        return request;
    }

    private boolean validateRequireFields() {

        return !(mTitleEditText.getText().toString().trim().isEmpty() || mDetailEditText.getText().toString().trim().isEmpty());
    }

    private class CustomTextWatcher implements TextWatcher {

        private CustomTextWatcher() {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {

            mOkButton.setEnabled(validateRequireFields());
        }
    }
}