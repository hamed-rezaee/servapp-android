package com.core.rezaee.servapp.file;

import com.core.rezaee.servapp.models.ContentModel;

import java.io.Serializable;

public class Content implements Serializable {

    public String type;
    public byte[] data;

    public Content(ContentModel model) {

        if (model == null) {
            return;
        }

        type = model.type;
        data = model.data;
    }

    public ContentModel getModel() {

        ContentModel model = new ContentModel();

        model.type = type;
        model.data = data;

        return model;
    }
}