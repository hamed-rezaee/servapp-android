package com.core.rezaee.servapp.network;

import android.support.annotation.StringRes;

import com.core.rezaee.servapp.R;

public enum Error {

    ERROR_UNKNOWN_ERROR(0, R.string.app_error__unknown_error),
    ERROR_SERVER_ERROR(1, R.string.app_error__server_error),
    ERROR_INVALID_DATA(2, R.string.app_error__invalid_data),
    ERROR_INVALID_USER_OR_PASSWORD(3, R.string.app_error__invalid_username_or_password),
    ERROR_USER_ALREADY_REGISTERED(4, R.string.app_error__user_already_registered),
    ERROR_INVALID_ACTIVATION_CODE(5, R.string.app_error_invalid_activation_code),
    ERROR_INVALID_PHONE_NO(6, R.string.app_error_invalid_phone_no),
    ERROR_USER_ALREADY_ACTIVATED(7, R.string.app_error_user_already_activated);

    private int mValue;
    private int mMessageResourceId;

    Error(int value, @StringRes int messageResourceId) {

        mValue = value;
        mMessageResourceId = messageResourceId;
    }

    public int getMessageResourceId() {

        return mMessageResourceId;
    }

    public static Error getServerError(int value) {

        for (Error error : Error.values()) {
            if (error.mValue == value) {
                return error;
            }
        }

        return null;
    }
}