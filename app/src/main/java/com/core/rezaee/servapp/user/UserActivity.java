package com.core.rezaee.servapp.user;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.core.rezaee.servapp.BaseActivity;
import com.core.rezaee.servapp.R;

public class UserActivity extends BaseActivity {

    private static final String KEY_USER_INTERACTION_MODE = "key_user_interaction_mode";

    private static final String TAG_CURRENT_FRAGMENT = "tag_current_fragment";

    private UserInteractionMode mInteractionMode = UserInteractionMode.LOGIN;

    private Fragment mCurrentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_user);

        if (savedInstanceState == null) {
            setFragment(mInteractionMode);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

        outState.putSerializable(KEY_USER_INTERACTION_MODE, mInteractionMode);
        getSupportFragmentManager().putFragment(outState, TAG_CURRENT_FRAGMENT, mCurrentFragment);
    }

    @Override
    public void initializeView() {
    }

    @Override
    public void setupView() {
    }

    public void onRestoreInstanceState(Bundle inState) {

        if (inState != null) {
            mInteractionMode = (UserInteractionMode) inState.getSerializable(KEY_USER_INTERACTION_MODE);
            mCurrentFragment = getSupportFragmentManager().getFragment(inState, TAG_CURRENT_FRAGMENT);
        }
    }

    public void setFragment(UserInteractionMode mode, Object... extraData) {

        switch (mode) {
            case LOGIN:
                mInteractionMode = UserInteractionMode.LOGIN;
                mCurrentFragment = new LoginFragment();

                break;
            case REGISTER:
                mInteractionMode = UserInteractionMode.REGISTER;
                mCurrentFragment = new RegisterFragment();

                break;
            case ACTIVATION_CODE:
                mInteractionMode = UserInteractionMode.ACTIVATION_CODE;
                mCurrentFragment = new ActivationCodeFragment();

                break;
            case FORGOT_ACCOUNT:
                mInteractionMode = UserInteractionMode.FORGOT_ACCOUNT;
                mCurrentFragment = new ForgotAccountFragment();

                break;
            case VERIFY:
                mInteractionMode = UserInteractionMode.VERIFY;
                mCurrentFragment = VerifyUserFragment.newInstance((String) extraData[0]);

                break;

            default:
                break;
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.activity_user__view_container, mCurrentFragment, TAG_CURRENT_FRAGMENT).commit();
    }
}