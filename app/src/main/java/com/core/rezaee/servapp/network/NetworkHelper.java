package com.core.rezaee.servapp.network;

import android.content.Context;

import com.core.rezaee.servapp.BuildConfig;
import com.core.rezaee.servapp.Constant;
import com.core.rezaee.servapp.ServappPreferenceManager;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkHelper {

    private static Retrofit mInstance = null;

    private NetworkHelper() {
    }

    public static synchronized Retrofit getInstance(final Context context) {

        if (mInstance == null) {
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

            httpClient.addInterceptor(chain -> {

                String token = ServappPreferenceManager.getUser(context) == null ? Constant.EMPTY_STRING : ServappPreferenceManager.getUser(context).token;
                Request request = chain.request().newBuilder().addHeader(Constant.KEY_HEADER_TOKEN_NAME, token).build();

                return chain.proceed(request);
            });

            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();

                loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                httpClient.addInterceptor(loggingInterceptor);
            }

            mInstance = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Constant.KEY_BASE_URL)
                    .client(httpClient.build())
                    .build();
        }

        return mInstance;
    }
}