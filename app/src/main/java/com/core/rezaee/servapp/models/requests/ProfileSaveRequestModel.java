package com.core.rezaee.servapp.models.requests;

public class ProfileSaveRequestModel {

    public String _id;
    public String firstName;
    public String lastName;
    public String nationalCode;
    public int score;
    public String userId;
    public String avatarId;
}