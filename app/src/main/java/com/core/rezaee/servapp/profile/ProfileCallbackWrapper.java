package com.core.rezaee.servapp.profile;

import com.core.rezaee.servapp.ServerError;

class ProfileCallbackWrapper implements ProfileCallback {

    @Override
    public void onException(int requestCode, ServerError serverError) {
    }

    @Override
    public void onProfileSaved(int requestCode) {
    }
}