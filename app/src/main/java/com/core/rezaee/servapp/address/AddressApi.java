package com.core.rezaee.servapp.address;

import com.core.rezaee.servapp.models.AddressModel;
import com.core.rezaee.servapp.models.requests.AddressSaveRequestModel;
import com.core.rezaee.servapp.models.requests.GenericIdRequestModel;
import com.core.rezaee.servapp.models.requests.ProfileAddressRequestModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AddressApi {

    @POST("addresses/get")
    Call<List<AddressModel>> getProfileAddresses(@Body ProfileAddressRequestModel requestModel);

    @POST("addresses/save")
    Call<Void> save(@Body AddressSaveRequestModel requestModel);

    @POST("addresses/delete")
    Call<Void> delete(@Body GenericIdRequestModel requestModel);
}