package com.core.rezaee.servapp.customviews;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.TypedValue;
import android.widget.ProgressBar;

import com.core.rezaee.servapp.Helper;
import com.core.rezaee.servapp.R;

import java.text.NumberFormat;

public class ProgressDialogFragment extends DialogFragment {

    private String mTitle;
    private String mMessage;
    private int mProgressStyle = ProgressDialog.STYLE_SPINNER;
    private boolean mIndeterminate = true;
    private boolean mCancelable = false;
    private ProgressDialog mProgressDialog;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        setRetainInstance(true);

        mProgressDialog = new ProgressDialog(getContext());

        mProgressDialog.setProgressStyle(mProgressStyle);
        mProgressDialog.setProgressNumberFormat(null);
        mProgressDialog.setIndeterminate(mIndeterminate);

        if (mIndeterminate) {
            mProgressDialog.setProgressPercentFormat(null);
        }

        mProgressDialog.setCanceledOnTouchOutside(false);
        setCancelable(mCancelable);

        if (mTitle != null) {
            mProgressDialog.setTitle(Helper.getCustomFontString(getActivity(), mTitle));
        }
        if (mMessage != null) {
            mProgressDialog.setMessage(Helper.getCustomFontString(getActivity(), mMessage));
        }

        if (mIndeterminate && mProgressStyle == ProgressDialog.STYLE_SPINNER) {
            mProgressDialog.setIndeterminateDrawable(getSpinnerDrawable(getContext()));
        }

        return mProgressDialog;
    }

    @Override
    public void onDestroyView() {

        Dialog dialog = getDialog();

        if (dialog != null && getRetainInstance()) {
            dialog.setDismissMessage(null);
        }

        super.onDestroyView();
    }

    @NonNull
    private Drawable getSpinnerDrawable(Context context) {

        Drawable spinnerDrawable;
        TypedValue typedValue = new TypedValue();
        ProgressBar progressBar = new ProgressBar(context);

        progressBar.setIndeterminate(true);

        spinnerDrawable = progressBar.getIndeterminateDrawable();
        context.getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true);
        spinnerDrawable.setColorFilter(typedValue.data, PorterDuff.Mode.MULTIPLY);

        return spinnerDrawable;
    }

    public ProgressDialogFragment setTitle(String title) {

        mTitle = title;

        if (mProgressDialog != null) {
            mProgressDialog.setTitle(title);
        }

        return this;
    }

    public ProgressDialogFragment setMessage(String message) {

        mMessage = message;

        if (mProgressDialog != null) {
            mProgressDialog.setMessage(message);
        }

        return this;
    }

    public ProgressDialogFragment setIndeterminate(boolean indeterminate) {

        mIndeterminate = indeterminate;

        if (mProgressDialog != null) {
            mProgressDialog.setIndeterminate(indeterminate);
        }

        return this;
    }

    public ProgressDialogFragment setProgressStyle(int progressStyle) {

        mProgressStyle = progressStyle;

        return this;
    }

    public ProgressDialogFragment setIsCancelable(boolean cancelable) {

        mCancelable = cancelable;

        return this;
    }

    public void setProgress(int progress) {

        if (mProgressDialog != null) {
            if (progress < 0) {
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setProgressPercentFormat(null);
                mIndeterminate = true;
            } else if (mProgressDialog.isIndeterminate()) {
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.setProgressPercentFormat(NumberFormat.getPercentInstance());
                mIndeterminate = false;
            }

            mProgressDialog.setProgress(progress);
        }
    }

    public boolean isShowing() {

        return mProgressDialog != null && mProgressDialog.isShowing();
    }
}