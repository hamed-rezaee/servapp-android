package com.core.rezaee.servapp.file;

import com.core.rezaee.servapp.ServerError;

public class FileCallbackWrapper implements FileCallback {

    @Override
    public void onException(int requestCode, ServerError serverError) {
    }

    @Override
    public void onFileDownloaded(int requestCode, File file) {
    }

    @Override
    public void onFileUploaded(int requestCode, File file) {
    }
}