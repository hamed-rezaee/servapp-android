package com.core.rezaee.servapp.address;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.core.rezaee.servapp.GenericIdRequest;
import com.core.rezaee.servapp.Helper;
import com.core.rezaee.servapp.R;
import com.core.rezaee.servapp.ServerError;
import com.core.rezaee.servapp.file.File;
import com.core.rezaee.servapp.file.FileCallback;
import com.core.rezaee.servapp.file.FileCallbackWrapper;

import java.util.List;

import timber.log.Timber;

import static com.core.rezaee.servapp.address.AddressFragment.REQUEST_CODE_ADDRESS_EDIT_DIALOG_FRAGMENT_RESULT;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

    private static final String LOGGER_TAG = "AddressAdapter";

    private static final String TAG_ADDRESS_EDIT_DIALOG_FRAGMENT = "tag_address_edit_dialog_fragment";

    public static final String KEY_ADDRESS = "key_address";
    public static final String KEY_STATIC_MAP = "key_static_map";

    private static final int REQUEST_CODE_DELETE_ADDRESS = 0;
    private static final int REQUEST_CODE_DOWNLOAD_STATIC_MAP = 1;

    private Context mContext;
    private Fragment mParentFragment;
    private List<Address> mDataSource;

    private int mSelectedPosition;

    private AddressCallback mAddressCallback = new AddressCallbackWrapper() {

        @Override
        public void onAddressesDeleted(int requestCode) {

            mDataSource.remove(mSelectedPosition);

            notifyItemRemoved(mSelectedPosition);
        }

        @Override
        public void onException(int requestCode, ServerError serverError) {

            Timber.e(serverError.message);

            Toast.makeText(mContext, mContext.getString(serverError.code.getMessageResourceId()), Toast.LENGTH_SHORT).show();
        }
    };

    public AddressAdapter(Context context, Fragment parentFragment, List<Address> addresses) {

        mContext = context;
        mParentFragment = parentFragment;
        mDataSource = addresses;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_address, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Address item = mDataSource.get(position);

        holder.titleTextView.setText(item.title);
        holder.rangeTextView.setText(mContext.getString(R.string.list_item_address__detail, item.range));
        holder.detailTextView.setText(mContext.getString(R.string.list_item_address__detail, item.detail));

        holder.staticMapImageView.setVisibility(View.INVISIBLE);
        holder.staticMapLoadingProgressBar.setVisibility(View.VISIBLE);

        File.download(mContext, REQUEST_CODE_DOWNLOAD_STATIC_MAP, holder.fileCallback, new GenericIdRequest(item.staticMap.id));

        preparePopupMenu(holder);
    }

    @Override
    public int getItemCount() {

        return mDataSource == null ? 0 : mDataSource.size();
    }

    public void setDataSource(List<Address> dataSource) {

        mDataSource = dataSource;
    }

    private void preparePopupMenu(@NonNull ViewHolder holder) {

        holder.optionsImageView.setOnClickListener(view -> {

            PopupMenu popupMenu = new PopupMenu(mContext, view);
            MenuInflater inflater = popupMenu.getMenuInflater();
            mSelectedPosition = holder.getAdapterPosition();

            popupMenu.setOnMenuItemClickListener(menuItem -> {

                switch (menuItem.getItemId()) {
                    case R.id.menu_address_items__item_delete:
                        Address.delete(mContext, REQUEST_CODE_DELETE_ADDRESS, mAddressCallback, new GenericIdRequest(mDataSource.get(mSelectedPosition).id));
                        break;
                    case R.id.menu_address_items__item_edit:
                        AddressEditDialogFragment addressEditDialogFragment = AddressEditDialogFragment.newInstance(mDataSource.get(mSelectedPosition));

                        addressEditDialogFragment.setTargetFragment(mParentFragment, REQUEST_CODE_ADDRESS_EDIT_DIALOG_FRAGMENT_RESULT);
                        addressEditDialogFragment.show(((FragmentActivity) mContext).getSupportFragmentManager(), TAG_ADDRESS_EDIT_DIALOG_FRAGMENT);
                        break;
                }

                return true;
            });

            inflater.inflate(R.menu.menu_address_items, popupMenu.getMenu());

            popupMenu.show();
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView titleTextView;
        private TextView rangeTextView;
        private TextView detailTextView;
        private ImageView optionsImageView;
        private ImageView staticMapImageView;
        private ProgressBar staticMapLoadingProgressBar;

        private FileCallback fileCallback = new FileCallbackWrapper() {

            @Override
            public void onFileDownloaded(int requestCode, File file) {

                staticMapImageView.setVisibility(View.VISIBLE);
                staticMapLoadingProgressBar.setVisibility(View.INVISIBLE);

                staticMapImageView.setImageBitmap(Helper.getBitmap(file.content.data));
            }

            @Override
            public void onException(int requestCode, ServerError serverError) {

                Timber.e(serverError.message);

                Toast.makeText(mContext, mContext.getString(serverError.code.getMessageResourceId()), Toast.LENGTH_SHORT).show();
            }
        };

        ViewHolder(View view) {

            super(view);

            titleTextView = view.findViewById(R.id.list_item_address__text_view_title);
            rangeTextView = view.findViewById(R.id.list_item_address__text_view_range);
            detailTextView = view.findViewById(R.id.list_item_address__text_view_detail);

            staticMapImageView = view.findViewById(R.id.list_item_address__image_view_static_map);
            optionsImageView = view.findViewById(R.id.list_item_address__image_view_options);

            staticMapLoadingProgressBar = view.findViewById(R.id.list_item_address__progress_bar_static_map_loading);
        }
    }
}