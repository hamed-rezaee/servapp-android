package com.core.rezaee.servapp.models;

import java.util.List;

public class ProfileModel {

    public String _id;
    public String firstName;
    public String lastName;
    public String nationalCode;
    public int score;

    public UserModel user;
    public FileModel avatar;
    public List<AddressModel> addresses;
}