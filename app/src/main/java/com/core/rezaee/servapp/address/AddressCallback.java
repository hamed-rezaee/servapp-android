package com.core.rezaee.servapp.address;

import com.core.rezaee.servapp.network.NetworkErrorCallback;

import java.util.List;

public interface AddressCallback extends NetworkErrorCallback {

    void onAddressesFetched(int requestCode, List<Address> addresses);

    void onAddressesSaved(int requestCode);

    void onAddressesDeleted(int requestCode);
}