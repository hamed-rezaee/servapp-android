package com.core.rezaee.servapp.models;

import com.core.rezaee.servapp.Exchangeable;
import com.core.rezaee.servapp.address.Address;

public class AddressModel implements Exchangeable<Address> {

    public String _id;
    public String title;
    public String detail;
    public String range;
    public double latitude;
    public double longitude;

    public ProfileModel profile;
    public FileModel staticMap;

    @Override
    public Address exchange(Object... extraData) {

        return new Address(this);
    }
}