package com.core.rezaee.servapp.address;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.core.rezaee.servapp.GenericIdRequest;
import com.core.rezaee.servapp.R;
import com.core.rezaee.servapp.ServerError;
import com.core.rezaee.servapp.profile.Profile;
import com.core.rezaee.servapp.profile.ProfileAddressRequest;
import com.core.rezaee.servapp.user.User;
import com.core.rezaee.servapp.user.UserCallback;
import com.core.rezaee.servapp.user.UserCallbackWrapper;

import java.util.List;

import timber.log.Timber;

import static com.core.rezaee.servapp.MainActivity.KEY_USER_ID;
import static com.core.rezaee.servapp.address.AddressActivity.KEY_PROFILE;

public class AddressFragment extends Fragment {

    private static final String LOGGER_TAG = "AddressFragment";

    private static final int REQUEST_CODE_GET_USER = 0;
    private static final int REQUEST_CODE_GET_ADDRESSES = 1;
    public static final int REQUEST_CODE_ADD_ADDRESSES = 2;
    public static final int REQUEST_CODE_ADDRESS_EDIT_DIALOG_FRAGMENT_RESULT = 3;

    private Profile mProfile;

    private RecyclerView mAddressRecyclerView;
    private AddressAdapter mAddressAdapter;
    private ProgressBar mLoadingProgressBar;
    private FloatingActionButton mAddressFab;

    private UserCallback mUserCallback = new UserCallbackWrapper() {

        @SuppressLint("RestrictedApi")
        @Override
        public void onUserFetched(int requestCode, User user) {

            mProfile = user.profile;

            mAddressFab.setVisibility(View.VISIBLE);

            Address.getProfileAddresses(getActivity(), REQUEST_CODE_GET_ADDRESSES, mAddressCallback, new ProfileAddressRequest(user.profile.id));

            mAddressRecyclerView.setVisibility(View.VISIBLE);
            mLoadingProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onException(int requestCode, ServerError serverError) {

            Timber.e(serverError.message);

            Toast.makeText(getActivity(), getString(serverError.code.getMessageResourceId()), Toast.LENGTH_SHORT).show();
        }
    };

    private AddressCallback mAddressCallback = new AddressCallbackWrapper() {

        @Override
        public void onAddressesFetched(int requestCode, List<Address> addresses) {

            mAddressAdapter.setDataSource(addresses);
            mAddressAdapter.notifyDataSetChanged();
        }

        @Override
        public void onException(int requestCode, ServerError serverError) {

            Timber.e(serverError.message);

            Toast.makeText(getActivity(), getString(serverError.code.getMessageResourceId()), Toast.LENGTH_SHORT).show();
        }
    };

    public static AddressFragment newInstance(String userId) {

        AddressFragment fragment = new AddressFragment();
        Bundle args = new Bundle();

        args.putSerializable(KEY_USER_ID, userId);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_address, container, false);

        initializeView(view);
        setupView();

        if (getArguments() != null) {
            User.getUser(getActivity(), REQUEST_CODE_GET_USER, mUserCallback, new GenericIdRequest(getArguments().getString(KEY_USER_ID)));
        }

        return view;
    }

    private void initializeView(View view) {

        mAddressRecyclerView = view.findViewById(R.id.fragment_address__recycler_view_address);

        mLoadingProgressBar = view.findViewById(R.id.fragment_address__progress_bar_loading);

        mAddressFab = view.findViewById(R.id.fragment_address__fab_add_address);
    }

    private void setupView() {

        mAddressRecyclerView.setVisibility(View.INVISIBLE);
        mLoadingProgressBar.setVisibility(View.VISIBLE);

        mAddressAdapter = new AddressAdapter(getActivity(), this, null);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        mAddressRecyclerView.setLayoutManager(mLayoutManager);
        mAddressRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAddressRecyclerView.setAdapter(mAddressAdapter);

        mAddressFab.setOnClickListener(view -> {

            Intent intent = new Intent(getActivity(), MapActivity.class);

            intent.putExtra(KEY_PROFILE, mProfile);

            startActivityForResult(intent, REQUEST_CODE_ADD_ADDRESSES);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (getActivity() == null) {
            return;
        }

        switch (requestCode) {
            case REQUEST_CODE_ADDRESS_EDIT_DIALOG_FRAGMENT_RESULT:
            case REQUEST_CODE_ADD_ADDRESSES:
                if (resultCode == Activity.RESULT_OK) {
                    Address.getProfileAddresses(getActivity(), REQUEST_CODE_GET_ADDRESSES, mAddressCallback, new ProfileAddressRequest(mProfile.id));
                }
                break;
        }
    }
}