package com.core.rezaee.servapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.core.rezaee.servapp.user.User;
import com.google.gson.Gson;

import static com.core.rezaee.servapp.Constant.KEY_PERSIAN;

@SuppressLint("ApplySharedPref")
public class ServappPreferenceManager {

    private static final String KEY_PREFERENCE_LANGUAGE = "key_preference_language";
    private static final String KEY_PREFERENCE_USER = "key_preference_user";

    private static SharedPreferences mInstance = null;

    private ServappPreferenceManager() {
    }

    private static synchronized SharedPreferences getInstance(Context context) {

        return mInstance == null ? PreferenceManager.getDefaultSharedPreferences(context) : mInstance;
    }

    public static void saveLanguage(Context context, String language) {

        getInstance(context).edit().putString(KEY_PREFERENCE_LANGUAGE, language).commit();
    }

    public static String getLanguage(Context context) {

        return getInstance(context).getString(KEY_PREFERENCE_LANGUAGE, KEY_PERSIAN);
    }

    public static void saveUser(Context context, User user) {

        getInstance(context).edit().putString(KEY_PREFERENCE_USER, new Gson().toJson(user, User.class)).commit();
    }

    public static User getUser(Context context) {

        return new Gson().fromJson(getInstance(context).getString(KEY_PREFERENCE_USER, Constant.EMPTY_STRING), User.class);
    }

    public static void clearUser(Context context) {

        getInstance(context).edit().remove(KEY_PREFERENCE_USER).commit();
    }
}