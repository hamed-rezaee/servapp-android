package com.core.rezaee.servapp.file;

import com.core.rezaee.servapp.models.FileModel;
import com.core.rezaee.servapp.models.requests.GenericIdRequestModel;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface FileApi {

    @POST("files/download")
    Call<FileModel> download(@Body GenericIdRequestModel requestModel);

    @Multipart
    @POST("files/upload/{id}")
    Call<FileModel> upload(@Path(value = "id", encoded = true) String id, @Part MultipartBody.Part content);
}