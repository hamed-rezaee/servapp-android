package com.core.rezaee.servapp.file;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.core.rezaee.servapp.GenericIdRequest;
import com.core.rezaee.servapp.Helper;
import com.core.rezaee.servapp.ServerError;
import com.core.rezaee.servapp.models.FileModel;
import com.core.rezaee.servapp.network.NetworkCallback;
import com.core.rezaee.servapp.network.NetworkHelper;

import java.io.Serializable;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

public class File implements Serializable {

    public String id;
    public Content content;

    public File(FileModel model) {

        if (model == null) {
            return;
        }

        id = model._id;
        content = new Content(model.content);
    }

    public FileModel getModel() {

        FileModel model = new FileModel();

        model._id = id;
        model.content = content.getModel();

        return model;
    }

    public static void download(Context context, final int requestCode, final FileCallback callback, GenericIdRequest request) {

        FileApi fileApi = NetworkHelper.getInstance(context).create(FileApi.class);
        Call<FileModel> call = fileApi.download(request.getModel());

        call.enqueue(new NetworkCallback<FileModel>(context) {

            @Override
            public void onResponse(@NonNull Call<FileModel> call, @NonNull Response<FileModel> response) {

                super.onResponse(call, response);

                if (!response.isSuccessful()) {
                    callback.onException(requestCode, ServerError.getError(response.errorBody()));
                } else {
                    callback.onFileDownloaded(requestCode, new File(response.body()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<FileModel> call, @NonNull Throwable e) {

                super.onFailure(call, e);

                callback.onException(requestCode, ServerError.getError(null));
            }
        });
    }

    public static void upload(Context context, final int requestCode, final FileCallback callback, String id, Uri fileUri) {

        java.io.File originalFile = FileHelper.getFile(context, fileUri);
        RequestBody file = RequestBody.create(MediaType.parse(Helper.getMimeType(context, fileUri)), originalFile);
        MultipartBody.Part content = MultipartBody.Part.createFormData("file", originalFile.getName(), file);

        FileApi fileApi = NetworkHelper.getInstance(context).create(FileApi.class);
        Call<FileModel> call = fileApi.upload(id, content);

        call.enqueue(new NetworkCallback<FileModel>(context, true) {

            @Override
            public void onResponse(@NonNull Call<FileModel> call, @NonNull Response<FileModel> response) {

                super.onResponse(call, response);

                if (!response.isSuccessful()) {
                    callback.onException(requestCode, ServerError.getError(response.errorBody()));
                } else {
                    callback.onFileUploaded(requestCode, new File(response.body()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<FileModel> call, @NonNull Throwable e) {

                super.onFailure(call, e);

                callback.onException(requestCode, ServerError.getError(null));
            }
        });
    }
}