package com.core.rezaee.servapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        BaseApplication.setupLocale(this);
        BaseApplication.setupFont(this);
    }

    public void setTitle(int title) {

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(Helper.getCustomFontString(this, getString(title)));
        }
    }

    public abstract void initializeView();

    public abstract void setupView();
}