package com.core.rezaee.servapp.user;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.core.rezaee.servapp.Constant;
import com.core.rezaee.servapp.MainActivity;
import com.core.rezaee.servapp.R;
import com.core.rezaee.servapp.ServappPreferenceManager;
import com.core.rezaee.servapp.ServerError;

import java.util.Arrays;

import timber.log.Timber;

import static com.core.rezaee.servapp.user.LoginFragment.KEY_USER;

public class VerifyUserFragment extends Fragment {

    private static final String LOGGER_TAG = "VerifyUserFragment";

    private static final String KEY_PHONE_NO = "key_phone_no";

    private static final int REQUEST_CODE_VERIFY_USER = 0;

    private String mPhoneNo;

    private TextView mCountdownTextView;

    private EditText mActivationCodeEditText;

    private Button mVerifyUserButton;

    public static VerifyUserFragment newInstance(String phoneNo) {

        Bundle args = new Bundle();
        VerifyUserFragment fragment = new VerifyUserFragment();

        args.putString(KEY_PHONE_NO, phoneNo);

        fragment.setArguments(args);

        return fragment;
    }

    private BroadcastReceiver receiveFromService = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle bundle = intent.getExtras();
            StringBuilder message = new StringBuilder(Constant.EMPTY_STRING);
            StringBuilder phoneNo = new StringBuilder(Constant.EMPTY_STRING);

            if (bundle != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");

                if (pdus != null) {
                    SmsMessage[] messages = new SmsMessage[pdus.length];

                    for (int i = 0; i < pdus.length; i++) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String format = bundle.getString("format");
                            messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                        } else {
                            messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        }

                        message.append(messages[i].getMessageBody());
                        phoneNo.append(messages[i].getOriginatingAddress());
                    }
                }
            }

            if (!TextUtils.isEmpty(phoneNo)) {
                fillActivationCode(message.toString(), phoneNo.toString());
            }
        }
    };

    UserCallback userCallback = new UserCallbackWrapper() {

        @Override
        public void onTokenFetched(int requestCode, User user) {

            if (getActivity() == null) {
                return;
            }

            ServappPreferenceManager.saveUser(getActivity(), user);

            Intent intent = new Intent(getActivity(), MainActivity.class);

            intent.putExtra(KEY_USER, user);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(intent);
        }

        @Override
        public void onException(int requestCode, ServerError serverError) {

            mVerifyUserButton.setEnabled(false);

            Timber.e(serverError.message);

            if (requestCode == REQUEST_CODE_VERIFY_USER) {
                Toast.makeText(getActivity(), getString(serverError.code.getMessageResourceId()), Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mPhoneNo = getArguments().getString(KEY_PHONE_NO, Constant.EMPTY_STRING);
        }

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_verify_user, container, false);

        initializeView(view);
        setupView();

        startCountdown();

        return view;
    }

    @Override
    public void onResume() {

        super.onResume();

        if (getActivity() == null) {
            return;
        }

        IntentFilter filter = new IntentFilter();

        filter.addAction("android.provider.Telephony.SMS_RECEIVED");

        getActivity().registerReceiver(receiveFromService, filter);
    }

    @Override
    public void onPause() {

        super.onPause();

        if (getActivity() == null) {
            return;
        }

        getActivity().unregisterReceiver(receiveFromService);
    }

    private void initializeView(View view) {

        mCountdownTextView = view.findViewById(R.id.fragment_verify_user__text_view_countdown);

        mActivationCodeEditText = view.findViewById(R.id.fragment_verify_user__edit_text_activation_code);

        mVerifyUserButton = view.findViewById(R.id.fragment_verify_user__button_verify_user);
    }

    private void setupView() {

        mVerifyUserButton.setEnabled(false);

        mActivationCodeEditText.addTextChangedListener(new CustomTextWatcher());

        mVerifyUserButton.setOnClickListener(view -> activate());
    }

    private void activate() {

        User.activate(getActivity(), REQUEST_CODE_VERIFY_USER, userCallback, getUserActivationRequest());
    }

    private UserActivationRequest getUserActivationRequest() {

        return new UserActivationRequest(mActivationCodeEditText.getText().toString().trim());
    }

    private boolean validateRequireFields() {

        return !(mActivationCodeEditText.getText().toString().trim().isEmpty());
    }

    private void startCountdown() {

        new CountDownTimer(Constant.VERIFY_USER_TIMEOUT_SECOND, Constant.DELAY_ONE_SECOND) {

            public void onTick(long millisUntilFinished) {

                try {
                    mCountdownTextView.setText(String.format(getString(R.string.fragment_verify_user__activation_code_countdown), millisUntilFinished / 1000, mPhoneNo));
                } catch (Exception ignored) {
                }
            }

            public void onFinish() {
            }
        }.start();
    }

    private void fillActivationCode(String message, String phoneNo) {

        if (Arrays.asList(Constant.SMS_PROVIDER_NUMBERS).contains(phoneNo)) {
            String[] splitMessage = message.split("\n");
            String activationCode = splitMessage[splitMessage.length - 1];

            mActivationCodeEditText.setText(activationCode);

            mCountdownTextView.setVisibility(View.INVISIBLE);
        }
    }

    private class CustomTextWatcher implements TextWatcher {

        private CustomTextWatcher() {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {

            mVerifyUserButton.setEnabled(validateRequireFields());
        }
    }
}