package com.core.rezaee.servapp.user;

import com.core.rezaee.servapp.models.requests.UserActivationRequestModel;

public class UserActivationRequest {

    public String activationCode;

    public UserActivationRequest(String activationCode) {

        this.activationCode = activationCode;
    }

    public UserActivationRequestModel getModel() {

        UserActivationRequestModel model = new UserActivationRequestModel();

        model.activationCode = activationCode;

        return model;
    }
}