package com.core.rezaee.servapp.customviews;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.ArrayRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.widget.TextView;

import com.core.rezaee.servapp.Helper;


public class AlertDialogFragment extends DialogFragment {

    enum ChoiceItemsType {

        ITEMS,
        SINGLE_CHOICE_ITEMS,
        MULTIPLE_CHOICE_ITEMS
    }

    protected String mTitle, mMessage, mPositiveTitle, mNegativeTitle;
    @StringRes
    protected int mPositiveTitleId, mNegativeTitleId, mTitleId, mMessageId;
    @ArrayRes
    protected int mItemsId;
    protected boolean mCanceledOnTouchOutside;
    protected CharSequence[] mItems;
    protected AlertDialog mAlertDialog;
    private int mSelectedSingleChoiceItem = -1;
    private boolean[] mCheckedItems;
    private ChoiceItemsType mChoiceItemsType;

    private DialogInterface.OnDismissListener mOnDismissListener;
    protected DialogInterface.OnClickListener mPositiveCallBack, mNegativeCallBack, mOnItemClickListener;

    public AlertDialogFragment setItems(@ArrayRes int itemsId, DialogInterface.OnClickListener onClickListener) {

        mItemsId = itemsId;
        mItems = null;
        mChoiceItemsType = ChoiceItemsType.ITEMS;

        mOnItemClickListener = onClickListener;

        return this;
    }

    public AlertDialogFragment setItems(CharSequence[] items, DialogInterface.OnClickListener onClickListener) {

        mItems = items;
        mItemsId = 0;
        mChoiceItemsType = ChoiceItemsType.ITEMS;

        mOnItemClickListener = onClickListener;

        return this;
    }

    public AlertDialogFragment setSingleChoiceItems(@ArrayRes int itemsId, int selectedIndex, DialogInterface.OnClickListener onClickListener) {

        mItemsId = itemsId;
        mItems = null;
        mSelectedSingleChoiceItem = selectedIndex;
        mChoiceItemsType = ChoiceItemsType.SINGLE_CHOICE_ITEMS;

        mOnItemClickListener = onClickListener;

        return this;
    }

    public AlertDialogFragment setSingleChoiceItems(CharSequence[] items, int selectedIndex, DialogInterface.OnClickListener onClickListener) {

        mItems = items;
        mItemsId = 0;
        mSelectedSingleChoiceItem = selectedIndex;
        mChoiceItemsType = ChoiceItemsType.SINGLE_CHOICE_ITEMS;

        mOnItemClickListener = onClickListener;

        return this;
    }

    public AlertDialogFragment setMultipleChoiceItems(@ArrayRes int itemsId, boolean[] checkedItems, DialogInterface.OnClickListener onClickListener) {

        mItemsId = itemsId;
        mItems = null;
        mCheckedItems = checkedItems;
        mChoiceItemsType = ChoiceItemsType.MULTIPLE_CHOICE_ITEMS;

        mOnItemClickListener = onClickListener;

        return this;
    }

    public AlertDialogFragment setMultipleChoiceItems(CharSequence[] items, boolean[] checkedItems, DialogInterface.OnClickListener onClickListener) {

        mItems = items;
        mItemsId = 0;
        mCheckedItems = checkedItems;
        mChoiceItemsType = ChoiceItemsType.MULTIPLE_CHOICE_ITEMS;

        mOnItemClickListener = onClickListener;

        return this;
    }

    public void setOnClickListenerOnShowingListDialog(final DialogInterface.OnClickListener onClickListener) {

        if (mAlertDialog == null || mAlertDialog.getListView() == null) {
            return;
        }

        mAlertDialog.getListView().setOnItemClickListener((parent, view, position, id) -> {

            if (onClickListener != null) {
                onClickListener.onClick(mAlertDialog, position);
            }

            mAlertDialog.dismiss();
        });
    }

    public AlertDialogFragment setCanceledOnTouchOutside(boolean canceledOnTouchOutside) {

        mCanceledOnTouchOutside = canceledOnTouchOutside;

        return this;
    }

    public AlertDialogFragment setTitle(String title) {

        mTitle = title;
        mTitleId = 0;

        return this;
    }

    public AlertDialogFragment setMessage(String message) {

        mMessage = message;
        mMessageId = 0;

        return this;
    }

    public AlertDialogFragment setTitle(@StringRes int titleId) {

        mTitleId = titleId;
        mTitle = null;

        return this;
    }

    public AlertDialogFragment setMessage(@StringRes int messageId) {

        mMessageId = messageId;
        mMessage = null;

        return this;
    }

    public void setPositiveButtonForExistingDialog(final DialogInterface.OnClickListener listener) {

        if (mAlertDialog == null || mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE) == null) {
            return;
        }

        mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(view -> {

            if (listener != null) {
                listener.onClick(mAlertDialog, 0);
            }

            mAlertDialog.dismiss();
        });
    }

    public void setNegativeButtonForExistingDialog(final DialogInterface.OnClickListener listener) {

        if (mAlertDialog == null || mAlertDialog.getButton(DialogInterface.BUTTON_NEGATIVE) == null) {
            return;
        }

        mAlertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setOnClickListener(view -> {

            if (listener != null) {
                listener.onClick(mAlertDialog, 0);
            }

            mAlertDialog.dismiss();
        });
    }

    public AlertDialogFragment setPositiveButton(String PositiveButtonTitle, DialogInterface.OnClickListener listener) {

        mPositiveTitle = PositiveButtonTitle;
        mPositiveCallBack = listener;

        return this;
    }

    public AlertDialogFragment setNegativeButton(String negativeButtonTitle, DialogInterface.OnClickListener listener) {

        mNegativeTitle = negativeButtonTitle;
        mNegativeCallBack = listener;

        return this;
    }

    public AlertDialogFragment setPositiveButton(DialogInterface.OnClickListener listener) {

        mPositiveCallBack = listener;

        return this;
    }

    public AlertDialogFragment setNegativeButton(DialogInterface.OnClickListener listener) {

        mNegativeCallBack = listener;

        return this;
    }

    public AlertDialogFragment setPositiveButtonTitle(String positiveButtonTitle) {

        mPositiveTitle = positiveButtonTitle;
        mPositiveTitleId = 0;

        return this;
    }

    public AlertDialogFragment setNegativeButtonTitle(String negativeButtonTitle) {

        mNegativeTitle = negativeButtonTitle;
        mNegativeTitleId = 0;

        return this;
    }

    public AlertDialogFragment setPositiveButtonTitle(@StringRes int positiveButtonTitleId) {

        mPositiveTitleId = positiveButtonTitleId;
        mPositiveTitle = null;

        return this;
    }

    public AlertDialogFragment setNegativeButtonTitle(@StringRes int negativeButtonTitleId) {

        mNegativeTitleId = negativeButtonTitleId;
        mNegativeTitle = null;

        return this;
    }

    public AlertDialogFragment setDismissListener(DialogInterface.OnDismissListener listener) {

        mOnDismissListener = listener;

        return this;
    }

    @NonNull
    @Override
    @SuppressLint("ResourceType")
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if (getActivity() == null) {
            return super.onCreateDialog(savedInstanceState);
        }

        setRetainInstance(true);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if (mTitle != null || mTitleId > 0) {
            builder.setTitle(Helper.getCustomFontString(getActivity(), mTitle != null ? mTitle : getString(mTitleId)));
        }

        if (mMessage != null || mMessageId > 0) {
            builder.setMessage(Helper.getCustomFontString(getActivity(), mMessage != null ? mMessage : getString(mMessageId)));
        }

        if (mPositiveTitle != null || mPositiveTitleId > 0) {
            builder.setPositiveButton(mPositiveTitle != null ? mPositiveTitle : getString(mPositiveTitleId), mPositiveCallBack);
        }

        if (mNegativeTitle != null || mNegativeTitleId > 0) {
            builder.setNegativeButton(mNegativeTitle != null ? mNegativeTitle : getString(mNegativeTitleId), mNegativeCallBack);
        }

        if (mOnItemClickListener != null) {
            if (mItemsId > 0) {
                if (mChoiceItemsType == ChoiceItemsType.SINGLE_CHOICE_ITEMS) {
                    builder.setSingleChoiceItems(mItems, mSelectedSingleChoiceItem, mOnItemClickListener);
                } else {
                    builder.setItems(mItemsId, mOnItemClickListener);
                }
            } else if (mItems != null) {
                if (mChoiceItemsType == ChoiceItemsType.SINGLE_CHOICE_ITEMS) {
                    builder.setSingleChoiceItems(mItems, mSelectedSingleChoiceItem, mOnItemClickListener);
                } else {
                    builder.setItems(mItems, mOnItemClickListener);
                }
            }
        }

        mAlertDialog = builder.create();
        mAlertDialog.setCanceledOnTouchOutside(mCanceledOnTouchOutside);

        mAlertDialog.setOnShowListener(dialogInterface -> {

            TextView titleTextView = mAlertDialog.findViewById(android.support.v7.appcompat.R.id.alertTitle);

            if (titleTextView != null) {
                titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            }

            TextView messageTextView = mAlertDialog.findViewById(android.R.id.message);

            if (messageTextView != null) {
                messageTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            }

            AlertDialogFragment.this.onShow();
        });

        return mAlertDialog;
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {

        super.onDismiss(dialog);

        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(dialog);
        }
    }

    @Override
    public void onDestroyView() {

        Dialog dialog = getDialog();

        if (dialog != null && getRetainInstance()) {
            dialog.setDismissMessage(null);
        }

        super.onDestroyView();
    }

    protected void onShow() {
    }
}