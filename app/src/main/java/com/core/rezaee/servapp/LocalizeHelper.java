package com.core.rezaee.servapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

public class LocalizeHelper {

    private static Locale getLocal(Context context) {

        return new Locale(ServappPreferenceManager.getLanguage(context));
    }

    public static void updateResourcesLocale(Application context) {

        Locale locale = getLocal(context);
        Configuration config = new Configuration();

        Locale.setDefault(locale);
        config.setLocale(locale);
        context.getBaseContext().getResources().updateConfiguration(config, context.getBaseContext().getResources().getDisplayMetrics());
    }

    public static void updateResourcesLocale(Context context) {

        Locale locale = getLocal(context);
        Configuration config = new Configuration();

        Locale.setDefault(locale);
        config.setLocale(locale);
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }

    public static boolean isLanguageEnglish(Context context) {

        return context.getResources().getConfiguration().locale.getLanguage().equals("en");
    }

    @SuppressLint("ApplySharedPref")
    public static void changeLocale(Activity activity, String language) {

        ServappPreferenceManager.saveLanguage(activity, language);

        activity.recreate();
    }
}