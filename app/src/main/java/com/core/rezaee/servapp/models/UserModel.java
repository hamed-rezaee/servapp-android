package com.core.rezaee.servapp.models;

public class UserModel {

    public String _id;
    public String username;
    public String phoneNo;
    public String password;
    public String code;
    public String presenterCode;

    public UserModel presenter;
    public ProfileModel profile;

    public String token;
}