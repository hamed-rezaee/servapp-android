package com.core.rezaee.servapp.user;

import android.content.Context;
import android.support.annotation.NonNull;

import com.core.rezaee.servapp.GenericIdRequest;
import com.core.rezaee.servapp.ServerError;
import com.core.rezaee.servapp.models.UserModel;
import com.core.rezaee.servapp.network.NetworkCallback;
import com.core.rezaee.servapp.network.NetworkHelper;
import com.core.rezaee.servapp.profile.Profile;

import java.io.Serializable;

import retrofit2.Call;
import retrofit2.Response;

public class User implements Serializable {

    private static final String LOGGER_TAG = "User";

    public String id;
    public String username;
    public String phoneNo;
    public String password;
    public String code;
    public String presenterCode;

    public User presenter;
    public Profile profile;

    public String token;

    public User(UserModel model) {

        if (model == null) {
            return;
        }

        id = model._id;
        username = model.username;
        phoneNo = model.phoneNo;
        code = model.code;
        presenterCode = model.presenterCode;

        presenter = new User(model.presenter);
        profile = new Profile(model.profile);

        token = model.token;
    }

    public UserModel getModel() {

        UserModel model = new UserModel();

        model._id = id;
        model.username = username;
        model.phoneNo = phoneNo;
        model.password = password;
        model.presenterCode = presenterCode;

        return model;
    }

    public static void register(Context context, final int requestCode, final UserCallback callback, UserRegisterRequest request) {

        UserApi userApi = NetworkHelper.getInstance(context).create(UserApi.class);
        Call<UserModel> call = userApi.registerUser(request.getModel());

        call.enqueue(new NetworkCallback<UserModel>(context, true) {

            @Override
            public void onResponse(@NonNull Call<UserModel> call, @NonNull Response<UserModel> response) {

                super.onResponse(call, response);

                if (!response.isSuccessful()) {
                    callback.onException(requestCode, ServerError.getError(response.errorBody()));
                } else {
                    callback.onUserRegistered(requestCode, new User(response.body()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserModel> call, @NonNull Throwable e) {

                super.onFailure(call, e);

                callback.onException(requestCode, ServerError.getError(null));
            }
        });
    }

    public static void getActivationCode(Context context, final int requestCode, final UserCallback callback, UserActivationCodeRequest request) {

        UserApi userApi = NetworkHelper.getInstance(context).create(UserApi.class);
        Call<Void> call = userApi.getActivationCode(request.getModel());

        call.enqueue(new NetworkCallback<Void>(context) {

            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {

                super.onResponse(call, response);

                if (!response.isSuccessful()) {
                    callback.onException(requestCode, ServerError.getError(response.errorBody()));
                } else {
                    callback.onActivationCodeFetched(requestCode);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable e) {

                super.onFailure(call, e);

                callback.onException(requestCode, ServerError.getError(null));
            }
        });
    }

    public static void login(Context context, final int requestCode, final UserCallback callback, UserLoginRequest request) {

        UserApi userApi = NetworkHelper.getInstance(context).create(UserApi.class);
        Call<UserModel> call = userApi.loginUser(request.getModel());

        call.enqueue(new NetworkCallback<UserModel>(context, true) {

            @Override
            public void onResponse(@NonNull Call<UserModel> call, @NonNull Response<UserModel> response) {

                super.onResponse(call, response);

                if (!response.isSuccessful()) {
                    callback.onException(requestCode, ServerError.getError(response.errorBody()));
                } else {
                    callback.onTokenFetched(requestCode, new User(response.body()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserModel> call, @NonNull Throwable e) {

                super.onFailure(call, e);

                callback.onException(requestCode, ServerError.getError(null));
            }
        });
    }

    public static void activate(Context context, final int requestCode, final UserCallback callback, UserActivationRequest request) {

        UserApi userApi = NetworkHelper.getInstance(context).create(UserApi.class);
        Call<UserModel> call = userApi.activateUser(request.getModel());

        call.enqueue(new NetworkCallback<UserModel>(context, true) {

            @Override
            public void onResponse(@NonNull Call<UserModel> call, @NonNull Response<UserModel> response) {

                super.onResponse(call, response);

                if (!response.isSuccessful()) {
                    callback.onException(requestCode, ServerError.getError(response.errorBody()));
                } else {
                    callback.onTokenFetched(requestCode, new User(response.body()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserModel> call, @NonNull Throwable e) {

                super.onFailure(call, e);

                callback.onException(requestCode, ServerError.getError(null));
            }
        });
    }

    public static void validatePhoneNo(Context context, final int requestCode, final UserCallback callback, UserActivationCodeRequest request) {

        UserApi userApi = NetworkHelper.getInstance(context).create(UserApi.class);
        Call<Void> call = userApi.validatePhoneNo(request.getModel());

        call.enqueue(new NetworkCallback<Void>(context, true) {

            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {

                super.onResponse(call, response);

                if (!response.isSuccessful()) {
                    callback.onException(requestCode, ServerError.getError(response.errorBody()));
                } else {
                    callback.onPhoneNoValidated(requestCode);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable e) {

                super.onFailure(call, e);

                callback.onException(requestCode, ServerError.getError(null));
            }
        });
    }

    public static void sendAccountInformation(Context context, final int requestCode, final UserCallback callback, UserActivationCodeRequest request) {

        UserApi userApi = NetworkHelper.getInstance(context).create(UserApi.class);
        Call<Void> call = userApi.sendAccountInformation(request.getModel());

        call.enqueue(new NetworkCallback<Void>(context, true) {

            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {

                super.onResponse(call, response);

                if (!response.isSuccessful()) {
                    callback.onException(requestCode, ServerError.getError(response.errorBody()));
                } else {
                    callback.onAccountInformationSent(requestCode);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable e) {

                super.onFailure(call, e);

                callback.onException(requestCode, ServerError.getError(null));
            }
        });
    }

    public static void getUser(Context context, final int requestCode, final UserCallback callback, GenericIdRequest request) {

        UserApi userApi = NetworkHelper.getInstance(context).create(UserApi.class);
        Call<UserModel> call = userApi.getUser(request.getModel());

        call.enqueue(new NetworkCallback<UserModel>(context) {

            @Override
            public void onResponse(@NonNull Call<UserModel> call, @NonNull Response<UserModel> response) {

                super.onResponse(call, response);

                if (!response.isSuccessful()) {
                    callback.onException(requestCode, ServerError.getError(response.errorBody()));
                } else {
                    callback.onUserFetched(requestCode, new User(response.body()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserModel> call, @NonNull Throwable e) {

                super.onFailure(call, e);

                callback.onException(requestCode, ServerError.getError(null));
            }
        });
    }
}