package com.core.rezaee.servapp.file;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.text.DecimalFormat;

public class FileHelper {

    static final String LOGGER_TAG = "FileHelper";

    public static final String MIME_TYPE_ALL = "*/*";
    public static final String MIME_TYPE_AUDIO = "audio/*";
    public static final String MIME_TYPE_TEXT = "text/*";
    public static final String MIME_TYPE_IMAGE = "image/*";
    public static final String MIME_TYPE_VIDEO = "video/*";
    public static final String MIME_TYPE_APP = "application/*";

    private FileHelper() {
    }

    private static final String HIDDEN_PREFIX = ".";

    public static String getExtension(String uri) {

        if (uri == null) {
            return null;
        }

        int dot = uri.lastIndexOf(".");

        if (dot >= 0) {
            return uri.substring(dot);
        } else {
            return "";
        }
    }

    private static boolean isLocal(String url) {

        return url != null && !url.startsWith("http://") && !url.startsWith("https://");

    }

    private static boolean isMediaUri(Uri uri) {

        return "media".equalsIgnoreCase(uri.getAuthority());
    }

    private static Uri getUri(File file) {

        if (file != null) {
            return Uri.fromFile(file);
        }

        return null;
    }

    public static File getPathWithoutFilename(File file) {

        if (file != null) {
            if (file.isDirectory()) {
                return file;
            } else {
                String filename = file.getName();
                String filepath = file.getAbsolutePath();

                String pathwithoutname = filepath.substring(0, filepath.length() - filename.length());

                if (pathwithoutname.endsWith("/")) {
                    pathwithoutname = pathwithoutname.substring(0, pathwithoutname.length() - 1);
                }

                return new File(pathwithoutname);
            }
        }

        return null;
    }

    public static String getMimeType(File file) {

        String extension = getExtension(file.getName());

        if (extension.length() > 0) {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.substring(1));
        }

        return "application/octet-stream";
    }

    public static String getMimeType(Context context, Uri uri) {

        File file = new File(getPath(context, uri));

        return getMimeType(file);
    }

    private static boolean isExternalStorageDocument(Uri uri) {

        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private static boolean isDownloadsDocument(Uri uri) {

        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private static boolean isMediaDocument(Uri uri) {

        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private static boolean isGooglePhotosUri(Uri uri) {

        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        final String column = "_data";

        final String[] projection = {
                column
        };

        try (Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null)) {

            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);

                return cursor.getString(column_index);
            }
        }

        return null;
    }

    public static String getPath(final Context context, final Uri uri) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;

                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";

                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static File getFile(Context context, Uri uri) {

        if (uri != null) {
            String path = getPath(context, uri);

            if (isLocal(path)) {
                return new File(path);
            }
        }

        return null;
    }

    public static String getReadableFileSize(int size) {

        final String KILOBYTES = " KB";
        final String MEGABYTES = " MB";
        final String GIGABYTES = " GB";
        final DecimalFormat decimalFormat = new DecimalFormat("###.#");

        float fileSize = 0;
        final int BYTES_IN_KILOBYTES = 1024;

        String suffix = KILOBYTES;

        if (size > BYTES_IN_KILOBYTES) {
            fileSize = size / BYTES_IN_KILOBYTES;

            if (fileSize > BYTES_IN_KILOBYTES) {
                fileSize = fileSize / BYTES_IN_KILOBYTES;

                if (fileSize > BYTES_IN_KILOBYTES) {
                    fileSize = fileSize / BYTES_IN_KILOBYTES;
                    suffix = GIGABYTES;
                } else {
                    suffix = MEGABYTES;
                }
            }
        }

        return String.valueOf(decimalFormat.format(fileSize) + suffix);
    }

    public static Intent createGetContentIntent(String mimeType) {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);

        intent.setType(mimeType);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        return intent;
    }
}