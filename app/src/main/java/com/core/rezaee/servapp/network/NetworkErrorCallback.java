package com.core.rezaee.servapp.network;

import com.core.rezaee.servapp.ServerError;

public interface NetworkErrorCallback {

    void onException(int requestCode, ServerError serverError);
}