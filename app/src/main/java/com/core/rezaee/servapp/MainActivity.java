package com.core.rezaee.servapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.core.rezaee.servapp.address.AddressActivity;
import com.core.rezaee.servapp.customviews.ConfirmDialogFragment;
import com.core.rezaee.servapp.profile.ProfileActivity;
import com.core.rezaee.servapp.user.User;
import com.core.rezaee.servapp.user.UserActivity;

import static com.core.rezaee.servapp.user.LoginFragment.KEY_USER;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final int REQUEST_CODE_SETTINGS_ACTIVITY = 0;

    public static final String KEY_USER_ID = "key_user_id";

    User mUser;

    private Toolbar mMainToolbar;
    private DrawerLayout mMainDrawerLayout;
    private NavigationView mMainNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        if (getIntent() != null) {

            mUser = (User) getIntent().getSerializableExtra(KEY_USER);
        }

        initializeView();
        setupView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == REQUEST_CODE_SETTINGS_ACTIVITY) {
            if (resultCode == RESULT_OK) {
                recreate();
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        Helper.hideDrawerLayout(mMainDrawerLayout);

        switch (menuItem.getItemId()) {
            case R.id.menu_navigation_view_items__item_home:
                break;
            case R.id.menu_navigation_view_items__item_addresses:
                showAddresses();
                break;
            case R.id.menu_navigation_view_items__item_profile:
                showProfile();
                break;
            case R.id.menu_navigation_view_items__item_settings:
                showSettings();
                break;
            case R.id.menu_navigation_view_items__item_logout:
                logout();
                break;
        }

        return true;
    }

    @Override
    public void initializeView() {

        mMainToolbar = findViewById(R.id.toolbar_main);
        mMainDrawerLayout = findViewById(R.id.activity_main__drawer_layout_main);
        mMainNavigationView = findViewById(R.id.activity_main__navigation_view_main);
    }

    @Override
    public void setupView() {

        setSupportActionBar(mMainToolbar);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mMainDrawerLayout, mMainToolbar, R.string.app__navigation_open, R.string.app__navigation_close);

        actionBarDrawerToggle.syncState();

        mMainDrawerLayout.addDrawerListener(actionBarDrawerToggle);
        mMainNavigationView.setNavigationItemSelectedListener(this);

        setTitle(R.string.app__home);
    }

    private void showAddresses() {

        Intent intent = new Intent(this, AddressActivity.class);

        intent.putExtra(KEY_USER_ID, mUser.id);

        startActivity(intent);
    }

    private void showProfile() {

        Intent intent = new Intent(this, ProfileActivity.class);

        intent.putExtra(KEY_USER_ID, mUser.id);

        startActivity(intent);
    }

    private void showSettings() {

        startActivityForResult(new Intent(this, SettingsActivity.class), REQUEST_CODE_SETTINGS_ACTIVITY);
    }

    private void logout() {

        ConfirmDialogFragment.getInstance().setPositiveButton(new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                ServappPreferenceManager.clearUser(MainActivity.this);

                startActivity(new Intent(MainActivity.this, UserActivity.class));

                System.exit(0);
            }
        }).show(getSupportFragmentManager(), Constant.TAG_CONFIRM_DIALOG);
    }
}