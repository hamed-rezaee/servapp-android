package com.core.rezaee.servapp.address;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cedarstudios.cedarmapssdk.CedarMaps;
import com.cedarstudios.cedarmapssdk.Dimension;
import com.cedarstudios.cedarmapssdk.MapView;
import com.cedarstudios.cedarmapssdk.listeners.ReverseGeocodeResultListener;
import com.cedarstudios.cedarmapssdk.listeners.StaticMapImageResultListener;
import com.cedarstudios.cedarmapssdk.model.geocoder.reverse.ReverseGeocode;
import com.core.rezaee.servapp.Helper;
import com.core.rezaee.servapp.R;
import com.core.rezaee.servapp.profile.Profile;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineListener;
import com.mapbox.android.core.location.LocationEnginePriority;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapboxMap;

import java.util.ArrayList;

import static com.core.rezaee.servapp.address.AddressActivity.KEY_PROFILE;

public class MapFragment extends Fragment implements LocationEngineListener {

    private static final String LOGGER_TAG = "MapFragment";

    private static final int REQUEST_CODE_LOCATION_PERMISSION = 0;
    private static final int REQUEST_CODE_ADDRESS_EDIT_DIALOG_FRAGMENT_RESULT = 1;

    private static final String TAG_ADDRESS_EDIT_DIALOG_FRAGMENT = "tag_address_edit_dialog_fragment";

    private static final LatLng VANAK_SQUARE_COORDINATION = new LatLng(35.7572, 51.4099);

    private static final int DEFAULT_ZOOM = 15;
    private static final int MAXIMUM_ZOOM = 17;
    private static final int MINIMUM_ZOOM = 6;

    private Profile mProfile;
    private LatLng mCoordinate;
    private ReverseGeocode mReverseGeocode;

    private MapView mMapView;
    private MapboxMap mMapboxMap;
    private LocationEngine mLocationEngine;

    private ReverseGeocodeResultListener mReverseGeocodeResultListener = new ReverseGeocodeResultListener() {

        @Override
        public void onSuccess(@NonNull ReverseGeocode reverseGeocode) {

            if (getActivity() == null) {
                return;
            }

            mReverseGeocode = reverseGeocode;

            Dimension mapDimension = new Dimension(800, 450);
            ArrayList<CedarMaps.StaticMarker> markers = new ArrayList<CedarMaps.StaticMarker>() {{
                add(new CedarMaps.StaticMarker(mCoordinate, null));
            }};

            CedarMaps.getInstance().staticMap(mapDimension, DEFAULT_ZOOM, mCoordinate, markers, mStaticMapImageResultListener);
        }

        @Override
        public void onFailure(@NonNull String errorMessage) {
        }
    };

    private StaticMapImageResultListener mStaticMapImageResultListener = new StaticMapImageResultListener() {

        @Override
        public void onSuccess(@NonNull Bitmap result) {

            if (getActivity() == null) {
                return;
            }

            AddressEditDialogFragment addressEditDialogFragment = AddressEditDialogFragment.newInstance(getAddress(), result);

            addressEditDialogFragment.setTargetFragment(MapFragment.this, REQUEST_CODE_ADDRESS_EDIT_DIALOG_FRAGMENT_RESULT);
            addressEditDialogFragment.show((getActivity()).getSupportFragmentManager(), TAG_ADDRESS_EDIT_DIALOG_FRAGMENT);
        }

        @Override
        public void onFailure(@NonNull String errorMessage) {
        }
    };

    public static MapFragment newInstance(Profile profile) {

        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();

        args.putSerializable(KEY_PROFILE, profile);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_map, container, false);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mProfile = (Profile) getArguments().getSerializable(KEY_PROFILE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        if (getActivity() == null) {
            return;
        }

        mMapView = view.findViewById(R.id.fragment_map__map_view_address);

        mMapView.onCreate(savedInstanceState);

        mMapView.getMapAsync(mapboxMap -> {

            mMapboxMap = mapboxMap;

            mMapboxMap.getUiSettings().setAttributionEnabled(false);
            mMapboxMap.getUiSettings().setLogoEnabled(false);

            mMapboxMap.setMaxZoomPreference(MAXIMUM_ZOOM);
            mMapboxMap.setMinZoomPreference(MINIMUM_ZOOM);

            mMapboxMap.setCameraPosition(new CameraPosition.Builder().target(VANAK_SQUARE_COORDINATION).zoom(DEFAULT_ZOOM).build());

            if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
                enableLocationComponent();
            }

            mMapboxMap.addOnMapClickListener(point -> {

                removeAllMarkersFromMapView();
                addMarkerToMapViewAtPosition(point);
            });

            setupCurrentLocationButton();
        });
    }

    private void removeAllMarkersFromMapView() {

        mMapboxMap.clear();
    }

    private void addMarkerToMapViewAtPosition(LatLng coordinate) {

        if (mMapboxMap != null) {
            mCoordinate = coordinate;

            mMapboxMap.addMarker(new MarkerOptions().position(mCoordinate));

            CedarMaps.getInstance().reverseGeocode(coordinate, mReverseGeocodeResultListener);
        }
    }

    private void animateToCoordinate(LatLng coordinate, int zoomLevel) {

        CameraPosition position = new CameraPosition.Builder().target(coordinate).zoom(zoomLevel).build();

        mMapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
    }

    @SuppressLint("MissingPermission")
    private void setupCurrentLocationButton() {

        if (getView() == null) {
            return;
        }

        FloatingActionButton currentLocationFab = getView().findViewById(R.id.fragment_map__fab_current_location);

        currentLocationFab.setOnClickListener(v -> {

            enableLocationComponent();
            toggleCurrentLocationButton();
        });
    }

    @SuppressLint("MissingPermission")
    private void toggleCurrentLocationButton() {

        if (!mMapboxMap.getLocationComponent().isLocationComponentEnabled()) {
            return;
        }

        Location location = mMapboxMap.getLocationComponent().getLastKnownLocation();

        if (location != null) {
            animateToCoordinate(new LatLng(location.getLatitude(), location.getLongitude()), DEFAULT_ZOOM);
        }

        switch (mMapboxMap.getLocationComponent().getRenderMode()) {
            case RenderMode.NORMAL:
                mMapboxMap.getLocationComponent().setRenderMode(RenderMode.COMPASS);
                break;
            case RenderMode.GPS:
                mMapboxMap.getLocationComponent().setRenderMode(RenderMode.NORMAL);
                break;
            case RenderMode.COMPASS:
                mMapboxMap.getLocationComponent().setRenderMode(RenderMode.NORMAL);
                break;
        }
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent() {

        if (getActivity() == null) {
            return;
        }

        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            initializeLocationEngine();

            mMapboxMap.getLocationComponent().activateLocationComponent(getActivity());
            mMapboxMap.getLocationComponent().setLocationComponentEnabled(true);
        } else {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION_PERMISSION);
        }
    }

    @SuppressWarnings({"MissingPermission"})
    private void initializeLocationEngine() {

        mLocationEngine = new LocationEngineProvider(getContext()).obtainBestLocationEngineAvailable();

        mLocationEngine.setPriority(LocationEnginePriority.HIGH_ACCURACY);
        mLocationEngine.activate();
        mLocationEngine.addLocationEngineListener(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {

        super.onSaveInstanceState(outState);

        mMapView.onSaveInstanceState(outState);
    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    public void onStart() {

        super.onStart();

        if (getActivity() == null) {
            return;
        }

        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            if (mLocationEngine != null) {
                mLocationEngine.activate();
            }
        }

        mMapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mLocationEngine != null) {
            mLocationEngine.removeLocationUpdates();
            mLocationEngine.removeLocationEngineListener(this);
        }

        mMapView.onStop();
    }

    @Override
    public void onResume() {

        super.onResume();

        mMapView.onResume();
    }

    @Override
    public void onPause() {

        super.onPause();

        mMapView.onPause();
    }

    @Override
    public void onLowMemory() {

        super.onLowMemory();

        mMapView.onLowMemory();
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();

        mMapView.onDestroy();

        if (mLocationEngine != null) {
            mLocationEngine.deactivate();
        }
    }

    @Override
    public void onDetach() {

        super.onDetach();

        mMapView = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_CODE_LOCATION_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enableLocationComponent();
                    toggleCurrentLocationButton();
                } else {
                    Toast.makeText(getActivity(), R.string.fragment_map__location_information_is_require, Toast.LENGTH_LONG).show();
                }
                break;

            default:
                break;
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onConnected() {

        if (getActivity() == null) {
            return;
        }

        if (mLocationEngine != null && PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            mLocationEngine.requestLocationUpdates();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    private Address getAddress() {

        Address address = new Address();

        address.range = Helper.getAddressFromReverseGeocode(mReverseGeocode);
        address.latitude = mCoordinate.getLatitude();
        address.longitude = mCoordinate.getLongitude();
        address.profile = mProfile;

        return address;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (getActivity() == null) {
            return;
        }

        switch (requestCode) {
            case REQUEST_CODE_ADDRESS_EDIT_DIALOG_FRAGMENT_RESULT:
                if (resultCode == Activity.RESULT_OK) {
                    getActivity().setResult(Activity.RESULT_OK, null);
                    getActivity().finish();
                }
                break;
        }
    }
}