package com.core.rezaee.servapp.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.core.rezaee.servapp.Constant;
import com.core.rezaee.servapp.Helper;
import com.core.rezaee.servapp.LocalizeHelper;
import com.core.rezaee.servapp.MainActivity;
import com.core.rezaee.servapp.R;
import com.core.rezaee.servapp.ServappPreferenceManager;
import com.core.rezaee.servapp.ServerError;

import timber.log.Timber;

import static com.core.rezaee.servapp.Constant.KEY_ENGLISH;
import static com.core.rezaee.servapp.Constant.KEY_PERSIAN;

public class LoginFragment extends Fragment {

    private static final String LOGGER_TAG = "LoginFragment";

    private static final int REQUEST_CODE_LOGIN_USER = 0;

    public static final String KEY_USER = "key_user";

    private EditText mUsernameEditText;
    private EditText mPasswordEditText;

    private Button mLoginButton;
    private Button mRegisterButton;
    private Button mForgotAccountTextView;

    private ImageView mLanguageImageView;

    UserCallback userCallback = new UserCallbackWrapper() {

        @Override
        public void onTokenFetched(int requestCode, User user) {

            if (getActivity() == null) {
                return;
            }

            ServappPreferenceManager.saveUser(getActivity(), user);

            Intent intent = new Intent(getActivity(), MainActivity.class);

            intent.putExtra(KEY_USER, user);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(intent);
        }

        @Override
        public void onException(int requestCode, ServerError serverError) {

            Timber.e(serverError.message);

            if (requestCode == REQUEST_CODE_LOGIN_USER) {
                Toast.makeText(getActivity(), getString(serverError.code.getMessageResourceId()), Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        initializeView(view);
        setupView();

        return view;
    }

    private void initializeView(View view) {

        mForgotAccountTextView = view.findViewById(R.id.fragment_login__text_view_forgot_account);

        mUsernameEditText = view.findViewById(R.id.fragment_login__edit_text_username);
        mPasswordEditText = view.findViewById(R.id.fragment_login__edit_text_password);

        mLoginButton = view.findViewById(R.id.fragment_login__button_login);
        mRegisterButton = view.findViewById(R.id.fragment_login__button_register);

        mLanguageImageView = view.findViewById(R.id.fragment_login__image_view_language);
    }

    private void setupView() {

        mLoginButton.setEnabled(false);

        mUsernameEditText.addTextChangedListener(new CustomTextWatcher());
        mPasswordEditText.addTextChangedListener(new CustomTextWatcher());

        mPasswordEditText.setOnEditorActionListener((view, actionId, event) -> {

            if (actionId == EditorInfo.IME_ACTION_DONE) {
                login();

                return true;
            } else {
                return false;
            }
        });

        mLoginButton.setOnClickListener(view -> login());

        mRegisterButton.setOnClickListener(view -> {

            if (getActivity() != null) {
                ((UserActivity) getActivity()).setFragment(UserInteractionMode.REGISTER);
            }
        });

        mForgotAccountTextView.setOnClickListener(view -> {

            if (getActivity() != null) {
                ((UserActivity) getActivity()).setFragment(UserInteractionMode.FORGOT_ACCOUNT);
            }
        });

        mLanguageImageView.setOnClickListener(view -> setLanguage());
    }

    private void login() {

        if (validateRequest()) {
            User.login(getActivity(), REQUEST_CODE_LOGIN_USER, userCallback, getLoginData());
        }
    }

    private UserLoginRequest getLoginData() {

        UserLoginRequest request = new UserLoginRequest();

        request.username = mUsernameEditText.getText().toString().trim();
        request.password = mPasswordEditText.getText().toString().trim();

        return request;
    }

    private boolean validateRequireFields() {

        return !(mUsernameEditText.getText().toString().trim().isEmpty() || mPasswordEditText.getText().toString().trim().isEmpty());
    }

    private boolean validateRequest() {

        Helper.hideKeypad(getActivity());

        return validateUsername() && validatePassword();
    }

    private boolean validateUsername() {

        if (mUsernameEditText.getText().toString().trim().length() < Constant.MIN_TEXT_CHARACTER_LENGTH) {
            String message = String.format(getString(R.string.app_validation__min_char_length), getString(R.string.fragment_login__username), String.valueOf(Constant.MIN_TEXT_CHARACTER_LENGTH));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mUsernameEditText);

            return false;
        }

        if (!Helper.checkUsername(mUsernameEditText.getText().toString().trim())) {
            String message = String.format(getString(R.string.app_validation__wrong_format), getString(R.string.fragment_login__username));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mUsernameEditText);

            return false;
        }

        return true;
    }

    private boolean validatePassword() {

        if (mPasswordEditText.getText().toString().trim().length() < Constant.MIN_PASSWORD_CHARACTER_LENGTH) {
            String message = String.format(getString(R.string.app_validation__min_char_length), getString(R.string.fragment_login__password), String.valueOf(Constant.MIN_PASSWORD_CHARACTER_LENGTH));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mPasswordEditText);

            return false;
        }

        if (!Helper.checkPasswordComplexity(mPasswordEditText.getText().toString().trim())) {
            Toast.makeText(getActivity(), getString(R.string.app_validation__password_complexity), Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mPasswordEditText);

            return false;
        }

        return true;
    }

    private void setLanguage() {

        if (getActivity() == null) {
            return;
        }

        LocalizeHelper.changeLocale(getActivity(), LocalizeHelper.isLanguageEnglish(getActivity()) ? KEY_PERSIAN : KEY_ENGLISH);
    }

    private class CustomTextWatcher implements TextWatcher {

        private CustomTextWatcher() {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {

            mLoginButton.setEnabled(validateRequireFields());
        }
    }
}