package com.core.rezaee.servapp;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;

import com.cedarstudios.cedarmapssdk.CedarMaps;

public class BaseApplication extends Application implements Application.ActivityLifecycleCallbacks {

    static {

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onCreate() {

        super.onCreate();

        initialize();

        registerActivityLifecycleCallbacks(this);
    }

    private void initialize() {

        setupLocale(this);
        setupFont(this);
        setupMapComponent(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);

        setupLocale(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {

        super.onTerminate();

        unregisterActivityLifecycleCallbacks(this);
    }

    public static void setupLocale(Application context) {

        LocalizeHelper.updateResourcesLocale(context);
    }

    public static void setupLocale(Context context) {

        LocalizeHelper.updateResourcesLocale(context);
    }

    public static void setupFont(Context context) {

        Helper.setFont(context);
    }

    public static void setupMapComponent(Context context) {

        CedarMaps.getInstance()
                .setClientID(Constant.KEY_MAP_CLIENT_ID)
                .setClientSecret(Constant.KEY_MAP_CLIENT_SECRET)
                .setContext(context);
    }
}