package com.core.rezaee.servapp.models.requests;

public class AddressSaveRequestModel {

    public String _id;
    public String title;
    public String detail;
    public String range;
    public double latitude;
    public double longitude;
    public String profileId;
    public String staticMapId;
}