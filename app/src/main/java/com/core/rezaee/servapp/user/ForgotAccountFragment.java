package com.core.rezaee.servapp.user;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.core.rezaee.servapp.Constant;
import com.core.rezaee.servapp.Helper;
import com.core.rezaee.servapp.R;
import com.core.rezaee.servapp.ServerError;

import timber.log.Timber;

public class ForgotAccountFragment extends Fragment {

    private static final String LOGGER_TAG = "ForgotAccountFragment";

    private static final int REQUEST_CODE_VALIDATE_PHONE_NO = 0;
    private static final int REQUEST_CODE_SEND_FORGOT_ACCOUNT = 1;

    private EditText mPhoneNoEditText;

    private Button mForgotAccountCodeButton;
    private Button mLoginButton;

    UserCallback userCallback = new UserCallbackWrapper() {

        @Override
        public void onPhoneNoValidated(int requestCode) {

            if (getActivity() == null) {
                return;
            }

            User.sendAccountInformation(getActivity(), REQUEST_CODE_SEND_FORGOT_ACCOUNT, userCallback, getActivationCodeRequest());
        }

        @Override
        public void onAccountInformationSent(int requestCode) {

            if (getActivity() == null) {
                return;
            }

            ((UserActivity) getActivity()).setFragment(UserInteractionMode.LOGIN);
        }

        @Override
        public void onException(int requestCode, ServerError serverError) {

            Timber.e(serverError.message);

            if (requestCode == REQUEST_CODE_VALIDATE_PHONE_NO) {
                Toast.makeText(getActivity(), getString(serverError.code.getMessageResourceId()), Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_forgot_account, container, false);

        initializeView(view);
        setupView();

        return view;
    }

    private void initializeView(View view) {

        mPhoneNoEditText = view.findViewById(R.id.fragment_forgot_account__edit_text_phone_no);

        mForgotAccountCodeButton = view.findViewById(R.id.fragment_forgot_account__button_forgot_account);
        mLoginButton = view.findViewById(R.id.fragment_forgot_account__button_login);
    }

    private void setupView() {

        mForgotAccountCodeButton.setEnabled(false);

        mPhoneNoEditText.addTextChangedListener(new CustomTextWatcher());

        mPhoneNoEditText.setOnEditorActionListener((view, actionId, event) -> {

            if (actionId == EditorInfo.IME_ACTION_DONE) {
                requestPhoneNo();

                return true;
            } else {
                return false;
            }
        });

        mForgotAccountCodeButton.setOnClickListener(view -> requestPhoneNo());

        mLoginButton.setOnClickListener(view -> {

            if (getActivity() != null) {
                ((UserActivity) getActivity()).setFragment(UserInteractionMode.LOGIN);
            }
        });
    }

    private void requestPhoneNo() {

        if (validateRequest()) {

            User.validatePhoneNo(getActivity(), REQUEST_CODE_VALIDATE_PHONE_NO, userCallback, getActivationCodeRequest());
        }
    }

    private UserActivationCodeRequest getActivationCodeRequest() {

        return new UserActivationCodeRequest(mPhoneNoEditText.getText().toString().trim());
    }

    private boolean validateRequest() {

        Helper.hideKeypad(getActivity());

        return validatePhoneNo();
    }

    private boolean validateRequireFields() {

        return !(mPhoneNoEditText.getText().toString().trim().isEmpty());
    }

    private boolean validatePhoneNo() {

        if (mPhoneNoEditText.getText().toString().trim().isEmpty()) {
            String message = String.format(getString(R.string.app_validation__required_field), getString(R.string.fragment_register__phone_no));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mPhoneNoEditText);

            return false;
        }

        if (mPhoneNoEditText.getText().toString().trim().length() < Constant.MIN_PHONE_NO_LENGTH) {
            String message = String.format(getString(R.string.app_validation__min_char_length), getString(R.string.fragment_register__phone_no), String.valueOf(Constant.MIN_PHONE_NO_LENGTH));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mPhoneNoEditText);

            return false;
        }

        if (!Helper.checkPhoneNo(mPhoneNoEditText.getText().toString().trim())) {
            String message = String.format(getString(R.string.app_validation__wrong_format), getString(R.string.fragment_register__phone_no));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mPhoneNoEditText);

            return false;
        }

        return true;
    }

    private class CustomTextWatcher implements TextWatcher {

        private CustomTextWatcher() {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {

            mForgotAccountCodeButton.setEnabled(validateRequireFields());
        }
    }
}