package com.core.rezaee.servapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;

import static com.core.rezaee.servapp.Constant.KEY_ENGLISH;
import static com.core.rezaee.servapp.Constant.KEY_PERSIAN;
import static com.core.rezaee.servapp.Constant.KEY_PREFERENCES_LANGUAGE;

public class SettingsFragment extends PreferenceFragment {

    SharedPreferences.OnSharedPreferenceChangeListener preferenceChangeListener = (sharedPreferences, key) -> {

        switch (key) {
            case KEY_PREFERENCES_LANGUAGE:
                Preference preference = findPreference(key);
                String languageValue = sharedPreferences.getString(key, Constant.KEY_PERSIAN);

                if (languageValue != null) {
                    preference.setSummary(getString(languageValue.equals(Constant.KEY_PERSIAN) ? R.string.app__persian : R.string.app__english));

                    if ((LocalizeHelper.isLanguageEnglish(getActivity()) && languageValue.equals(Constant.KEY_PERSIAN)) || (!LocalizeHelper.isLanguageEnglish(getActivity()) && languageValue.equals(Constant.KEY_ENGLISH))) {
                        ((SettingsActivity) getActivity()).needsToReload = !((SettingsActivity) getActivity()).needsToReload;
                        LocalizeHelper.changeLocale(getActivity(), languageValue.equals(Constant.KEY_PERSIAN) ? KEY_PERSIAN : KEY_ENGLISH);
                    }
                }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.prefrences);

        setupView();
    }

    @Override
    public void onResume() {

        super.onResume();

        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(preferenceChangeListener);

        Preference preference = findPreference(KEY_PREFERENCES_LANGUAGE);
        String languageValue = getPreferenceScreen().getSharedPreferences().getString(KEY_PREFERENCES_LANGUAGE, Constant.KEY_PERSIAN);

        if (languageValue != null) {
            preference.setSummary(getString(languageValue.equals(Constant.KEY_PERSIAN) ? R.string.app__persian : R.string.app__english));
        }
    }

    @Override
    public void onPause() {

        super.onPause();

        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(preferenceChangeListener);
    }

    private void setupView() {

        ((DialogPreference) findPreference(KEY_PREFERENCES_LANGUAGE)).setNegativeButtonText(R.string.app__cancel);
    }
}