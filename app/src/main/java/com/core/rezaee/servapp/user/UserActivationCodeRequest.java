package com.core.rezaee.servapp.user;

import com.core.rezaee.servapp.models.requests.UserActivationCodeRequestModel;

public class UserActivationCodeRequest {

    public String phoneNo;

    public UserActivationCodeRequest(String phoneNo) {

        this.phoneNo = phoneNo;
    }

    public UserActivationCodeRequestModel getModel() {

        UserActivationCodeRequestModel model = new UserActivationCodeRequestModel();

        model.phoneNo = phoneNo;

        return model;
    }
}