package com.core.rezaee.servapp.models.requests;

public class UserRegisterRequestModel {

    public String username;
    public String phoneNo;
    public String password;
    public String presenterCode;
}