package com.core.rezaee.servapp.user;

import com.core.rezaee.servapp.models.UserModel;
import com.core.rezaee.servapp.models.requests.GenericIdRequestModel;
import com.core.rezaee.servapp.models.requests.UserActivationCodeRequestModel;
import com.core.rezaee.servapp.models.requests.UserActivationRequestModel;
import com.core.rezaee.servapp.models.requests.UserLoginRequestModel;
import com.core.rezaee.servapp.models.requests.UserRegisterRequestModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserApi {

    @POST("users/get")
    Call<UserModel> getUser(@Body GenericIdRequestModel requestModel);

    @POST("users/register")
    Call<UserModel> registerUser(@Body UserRegisterRequestModel requestModel);

    @POST("users/login")
    Call<UserModel> loginUser(@Body UserLoginRequestModel requestModel);

    @POST("users/activate")
    Call<UserModel> activateUser(@Body UserActivationRequestModel requestModel);

    @POST("users/activationCode")
    Call<Void> getActivationCode(@Body UserActivationCodeRequestModel requestModel);

    @POST("users/validatePhoneNo")
    Call<Void> validatePhoneNo(@Body UserActivationCodeRequestModel requestModel);

    @POST("users/sendAccountInformation")
    Call<Void> sendAccountInformation(@Body UserActivationCodeRequestModel requestModel);
}