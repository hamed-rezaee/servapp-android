package com.core.rezaee.servapp.customviews;

import android.annotation.SuppressLint;

import com.core.rezaee.servapp.R;

public class ConfirmDialogFragment extends AlertDialogFragment {

    public static ConfirmDialogFragment getInstance() {

        return (ConfirmDialogFragment) new ConfirmDialogFragment()
                .setMessage(R.string.dialog__message_confirm)
                .setPositiveButtonTitle(R.string.dialog__positive_button_title_yes)
                .setNegativeButtonTitle(R.string.dialog__negative_button_title_no)
                .setCanceledOnTouchOutside(true);
    }

    @SuppressLint("ValidFragment")
    private ConfirmDialogFragment() {
    }
}