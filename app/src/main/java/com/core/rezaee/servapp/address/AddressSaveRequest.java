package com.core.rezaee.servapp.address;

import com.core.rezaee.servapp.models.requests.AddressSaveRequestModel;

public class AddressSaveRequest {

    public String id;
    public String title;
    public String detail;
    public String range;
    public double latitude;
    public double longitude;
    public String profileId;
    public String staticMapId;

    public AddressSaveRequestModel getModel() {

        AddressSaveRequestModel model = new AddressSaveRequestModel();

        model._id = id;
        model.title = title;
        model.detail = detail;
        model.range = range;
        model.latitude = latitude;
        model.longitude = longitude;
        model.profileId = profileId;
        model.staticMapId = staticMapId;

        return model;
    }
}