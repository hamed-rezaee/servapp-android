package com.core.rezaee.servapp;

public interface Exchangeable<T> {

    T exchange(Object... extraData);
}