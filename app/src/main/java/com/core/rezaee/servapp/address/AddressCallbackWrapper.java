package com.core.rezaee.servapp.address;

import com.core.rezaee.servapp.ServerError;

import java.util.List;

class AddressCallbackWrapper implements AddressCallback {

    @Override
    public void onException(int requestCode, ServerError serverError) {
    }

    @Override
    public void onAddressesFetched(int requestCode, List<Address> addresses) {
    }

    @Override
    public void onAddressesSaved(int requestCode) {
    }

    @Override
    public void onAddressesDeleted(int requestCode) {
    }
}