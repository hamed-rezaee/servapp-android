package com.core.rezaee.servapp.user;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.core.rezaee.servapp.Constant;
import com.core.rezaee.servapp.Helper;
import com.core.rezaee.servapp.R;
import com.core.rezaee.servapp.ServerError;

import timber.log.Timber;

public class ActivationCodeFragment extends Fragment {

    private static final String LOGGER_TAG = "ActivationCodeFragment";

    private static final int REQUEST_CODE_RECEIVE_SMS = 0;
    private static final int REQUEST_CODE_VALIDATE_PHONE_NO = 1;
    private static final int REQUEST_CODE_GET_ACTIVATION_CODE = 2;

    private EditText mPhoneNoEditText;
    private Button mActivationCodeButton;
    private Button mRegisterButton;

    UserCallback userCallback = new UserCallbackWrapper() {

        @Override
        public void onPhoneNoValidated(int requestCode) {

            if (getActivity() == null) {
                return;
            }

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.RECEIVE_SMS}, REQUEST_CODE_RECEIVE_SMS);
            } else {
                getActivationCode();
            }
        }

        @Override
        public void onActivationCodeFetched(int requestCode) {

            if (getActivity() == null) {
                return;
            }

            ((UserActivity) getActivity()).setFragment(UserInteractionMode.VERIFY, mPhoneNoEditText.getText().toString().trim());
        }

        @Override
        public void onException(int requestCode, ServerError serverError) {

            Timber.e(serverError.message);

            if (requestCode == REQUEST_CODE_VALIDATE_PHONE_NO || requestCode == REQUEST_CODE_GET_ACTIVATION_CODE) {
                Toast.makeText(getActivity(), getString(serverError.code.getMessageResourceId()), Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_activation_code, container, false);

        initializeView(view);
        setupView();

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_CODE_RECEIVE_SMS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getActivationCode();
                }

                break;
        }
    }

    private void initializeView(View view) {

        mPhoneNoEditText = view.findViewById(R.id.fragment_activation_code__edit_text_phone_no);

        mActivationCodeButton = view.findViewById(R.id.fragment_activation_code__button_activation_code);
        mRegisterButton = view.findViewById(R.id.fragment_activation_code__button_register);
    }

    private void setupView() {

        mActivationCodeButton.setEnabled(false);

        mPhoneNoEditText.addTextChangedListener(new CustomTextWatcher());

        mPhoneNoEditText.setOnEditorActionListener((view, actionId, event) -> {

            if (actionId == EditorInfo.IME_ACTION_DONE) {
                requestPhoneNo();

                return true;
            } else {
                return false;
            }
        });

        mActivationCodeButton.setOnClickListener(view -> requestPhoneNo());

        mRegisterButton.setOnClickListener(view -> {

            if (getActivity() != null) {
                ((UserActivity) getActivity()).setFragment(UserInteractionMode.REGISTER);
            }
        });
    }

    private void requestPhoneNo() {

        if (validateRequest()) {
            User.validatePhoneNo(getActivity(), REQUEST_CODE_VALIDATE_PHONE_NO, userCallback, getActivationCodeRequest());
        }
    }

    private UserActivationCodeRequest getActivationCodeRequest() {

        return new UserActivationCodeRequest(mPhoneNoEditText.getText().toString().trim());
    }

    private boolean validateRequireFields() {

        return !(mPhoneNoEditText.getText().toString().trim().isEmpty());
    }

    private boolean validateRequest() {

        Helper.hideKeypad(getActivity());

        return validatePhoneNo();
    }

    private boolean validatePhoneNo() {

        if (mPhoneNoEditText.getText().toString().trim().isEmpty()) {
            String message = String.format(getString(R.string.app_validation__required_field), getString(R.string.fragment_register__phone_no));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mPhoneNoEditText);

            return false;
        }

        if (mPhoneNoEditText.getText().toString().trim().length() < Constant.MIN_PHONE_NO_LENGTH) {
            String message = String.format(getString(R.string.app_validation__min_char_length), getString(R.string.fragment_register__phone_no), String.valueOf(Constant.MIN_PHONE_NO_LENGTH));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mPhoneNoEditText);

            return false;
        }

        if (!Helper.checkPhoneNo(mPhoneNoEditText.getText().toString().trim())) {
            String message = String.format(getString(R.string.app_validation__wrong_format), getString(R.string.fragment_register__phone_no));

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            Helper.requestFocus(getActivity(), mPhoneNoEditText);

            return false;
        }

        return true;
    }

    private void getActivationCode() {

        if (getActivity() == null) {
            return;
        }

        User.getActivationCode(getActivity(), REQUEST_CODE_GET_ACTIVATION_CODE, userCallback, getActivationCodeRequest());
    }

    private class CustomTextWatcher implements TextWatcher {

        private CustomTextWatcher() {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {

            mActivationCodeButton.setEnabled(validateRequireFields());
        }
    }
}