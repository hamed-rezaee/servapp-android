package com.core.rezaee.servapp.address;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import com.cedarstudios.cedarmapssdk.CedarMaps;
import com.cedarstudios.cedarmapssdk.listeners.OnTilesConfigured;
import com.core.rezaee.servapp.BaseActivity;
import com.core.rezaee.servapp.R;
import com.core.rezaee.servapp.profile.Profile;

import static com.core.rezaee.servapp.address.AddressActivity.KEY_PROFILE;

public class MapActivity extends BaseActivity {

    private static final String TAG_CURRENT_FRAGMENT = "tag_current_fragment";

    private Toolbar mMainToolbar;
    private Fragment mCurrentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_map);

        CedarMaps.getInstance().prepareTiles(new OnTilesConfigured() {

            @Override
            public void onSuccess() {

                if (savedInstanceState == null) {

                    Profile profile = null;

                    if (getIntent().getExtras() != null) {
                        profile = (Profile) getIntent().getExtras().getSerializable(KEY_PROFILE);
                    }

                    setFragment(profile);
                }

                initializeView();
                setupView();
            }

            @Override
            public void onFailure(@NonNull String error) {
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

        getSupportFragmentManager().putFragment(outState, TAG_CURRENT_FRAGMENT, mCurrentFragment);
    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();

        return true;
    }

    @Override
    public void initializeView() {

        mMainToolbar = findViewById(R.id.toolbar_main);
    }

    @Override
    public void setupView() {

        setSupportActionBar(mMainToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        setTitle(R.string.activity_map__location_select);
    }

    public void onRestoreInstanceState(Bundle inState) {

        if (inState != null) {
            mCurrentFragment = getSupportFragmentManager().getFragment(inState, TAG_CURRENT_FRAGMENT);
        }
    }

    public void setFragment(Profile profile) {

        mCurrentFragment = MapFragment.newInstance(profile);

        getSupportFragmentManager().beginTransaction().replace(R.id.activity_map__view_container, mCurrentFragment, TAG_CURRENT_FRAGMENT).commit();
    }
}