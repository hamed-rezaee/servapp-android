package com.core.rezaee.servapp.user;

import com.core.rezaee.servapp.models.requests.UserLoginRequestModel;

public class UserLoginRequest {

    public String username;
    public String password;

    public UserLoginRequestModel getModel() {

        UserLoginRequestModel model = new UserLoginRequestModel();

        model.username = username;
        model.password = password;

        return model;
    }
}