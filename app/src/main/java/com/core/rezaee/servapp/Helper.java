package com.core.rezaee.servapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.StringRes;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;

import com.cedarstudios.cedarmapssdk.model.geocoder.reverse.ReverseGeocode;
import com.core.rezaee.servapp.customviews.TypefaceSpan;
import com.core.rezaee.servapp.user.User;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class Helper {

    private final static char[] PERSIAN_DIGITS = {'۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'};

    public static String getLocalNumber(int number) {

        return getPersianString(String.valueOf(number));
    }

    public static String getPersianString(String input) {

        if (Locale.getDefault().getLanguage().equals("fa")) {
            StringBuilder sb = new StringBuilder();

            for (char i : input.toCharArray()) {
                sb.append(Character.isDigit(i) ? PERSIAN_DIGITS[Integer.parseInt(String.valueOf(i))] : i);
            }

            return sb.toString();
        } else {
            return input;
        }
    }

    public static String formatDate(long date) {

        return new SimpleDateFormat(Constant.DATE_FORMAT).format(date);
    }

    public static String getTime(long date) {

        return new SimpleDateFormat(Constant.TIME_FORMAT).format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public static String formatDate(Date date) {

        return new SimpleDateFormat(Constant.DATE_FORMAT).format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public static String getTime(Date date) {

        return new SimpleDateFormat(Constant.TIME_FORMAT).format(date);
    }

    public static void hideKeypad(Activity activity) {

        hideKeypad(activity, activity.getCurrentFocus());
    }

    public static void hideKeypad(Context context, View view) {

        try {
            final InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(android.content.Context.INPUT_METHOD_SERVICE);

            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception ignored) {
        }
    }

    public static void hideDrawerLayout(DrawerLayout drawerLayout) {

        drawerLayout.closeDrawer(GravityCompat.START);
    }

    public static String formatDecimalForDisplay(double value) {

        return formatDecimalForDisplay(value, false);
    }

    public static String formatDecimalForDisplay(double value, boolean roundValue) {

        String basePattern = "#,###.##";
        Locale defaultLocale = Locale.getDefault();
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(defaultLocale);
        String formatPattern = basePattern + ";" + (defaultLocale.getLanguage().equals("fa") ? basePattern + " -" : "- " + basePattern);

        decimalFormat.applyPattern(formatPattern);

        value = roundValue ? Math.round(value) : value;

        return decimalFormat.format(value);
    }

    public static long getMillisDate(Date date) {

        return date.getTime();
    }

    public static Date getDate(long millis) {

        return new Date(millis);
    }

    public static void requestFocus(Activity activity, View view) {

        if (view.requestFocus()) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId(Context context) {

        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static <T> List<T> exchange(Exchangeable<T>[] exchangeables, Object... extraData) {

        if (exchangeables == null) {
            return null;
        }

        List<T> output = new ArrayList<>(exchangeables.length);

        for (Exchangeable<T> model : exchangeables) {
            T outputItem = model.exchange(extraData);

            if (outputItem != null) {
                output.add(outputItem);
            }
        }

        return output;
    }

    public static <T> List<T> exchange(List<? extends Exchangeable<T>> exchangeables, Object... extraData) {

        if (exchangeables == null) {
            return null;
        }

        List<T> output = new ArrayList<>(exchangeables.size());

        for (Exchangeable<T> model : exchangeables) {
            output.add(model.exchange(extraData));
        }

        return output;
    }

    public static Typeface getTypeface(Context context) {

        return Typeface.createFromAsset(context.getApplicationContext().getAssets(), Constant.FONT_FA_PATH);
    }

    public static SpannableString getCustomFontString(Context context, @StringRes int resourceId) {

        return getCustomFontString(context, context.getString(resourceId));
    }

    public static SpannableString getCustomFontString(Context context, String input) {

        if (context == null || input == null) {
            return null;
        }

        SpannableString output = new SpannableString(input);

        output.setSpan(new TypefaceSpan(context), 0, output.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return output;
    }

    public static void setFont(Context context) {

        final Typeface regular = Typeface.createFromAsset(context.getAssets(), Constant.FONT_FA_PATH);

        replaceFont("SERIF", regular);
        replaceFont("DEFAULT", regular);
        replaceFont("MONOSPACE", regular);
        replaceFont("SANS_SERIF", regular);
    }

    private static void replaceFont(String staticTypefaceFieldName, Typeface newTypeface) {

        try {
            Field staticField = Typeface.class.getDeclaredField(staticTypefaceFieldName);

            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {
        }
    }

    public static boolean checkPasswordComplexity(String string) {

        String pattern = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).*$";

        return string.matches(pattern);
    }

    public static boolean checkUsername(String string) {

        String pattern = "^([a-zA-Z0-9_.])*$";

        return string.matches(pattern);
    }

    public static boolean checkPhoneNo(String string) {

        String pattern = "^0\\d*$";

        return string.matches(pattern);
    }

    public static void setStatusBarTransparent(Activity activity) {

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager != null) {
            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

            return (activeNetwork != null && activeNetwork.isConnected());
        } else {
            return false;
        }
    }

    public User decoded(String token) {

        return new Gson().fromJson(getPayload(token), User.class);
    }

    private String getPayload(String token) {

        return new String(Base64.decode(token.split("\\.")[1], Base64.URL_SAFE), StandardCharsets.UTF_8);
    }

    public static Bitmap getBitmap(byte[] data) {

        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }

    public static String getAddressFromReverseGeocode(ReverseGeocode reverseGeocode) {

        String result = Constant.EMPTY_STRING;

        if (!TextUtils.isEmpty(reverseGeocode.getProvince())) {
            result += reverseGeocode.getProvince();
        }

        if (!TextUtils.isEmpty(reverseGeocode.getCity())) {
            if (TextUtils.isEmpty(result)) {
                result = reverseGeocode.getCity();
            } else {
                result = result + ", " + reverseGeocode.getCity();
            }
        }

        if (!TextUtils.isEmpty(reverseGeocode.getLocality())) {
            if (TextUtils.isEmpty(result)) {
                result = reverseGeocode.getLocality();
            } else {
                result = result + ", " + reverseGeocode.getLocality();
            }
        }

        if (!TextUtils.isEmpty(reverseGeocode.getAddress())) {
            if (TextUtils.isEmpty(result)) {
                result = reverseGeocode.getAddress();
            } else {
                result = result + ", " + reverseGeocode.getAddress();
            }
        }

        if (!TextUtils.isEmpty(reverseGeocode.getPlace())) {
            if (TextUtils.isEmpty(result)) {
                result = reverseGeocode.getPlace();
            } else {
                result = result + ", " + reverseGeocode.getPlace();
            }
        }

        return result;
    }

    public static Uri getStaticMapUri(Context context, Bitmap mBitmap) {

        String fileName = "static_map.jpg";
        File file = new File(context.getExternalCacheDir(), fileName);

        if (file.exists())
            file.delete();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);

            mBitmap.compress(Bitmap.CompressFormat.JPEG, 70, fileOutputStream);

            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception ignored) {
        }

        return Uri.fromFile(file);
    }

    public static String getMimeType(Context context, Uri uri) {

        String mimeType;

        if (Objects.equals(uri.getScheme(), ContentResolver.SCHEME_CONTENT)) {
            mimeType = context.getContentResolver().getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());
        }

        return mimeType;
    }
}