package com.core.rezaee.servapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.core.rezaee.servapp.user.User;
import com.core.rezaee.servapp.user.UserActivity;

import static com.core.rezaee.servapp.user.LoginFragment.KEY_USER;

public class SplashActivity extends BaseActivity {

    private ImageView mLogoImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        Helper.setStatusBarTransparent(this);

        initializeView();
        setupView();
    }

    @Override
    public void initializeView() {

        mLogoImageView = findViewById(R.id.activity_splash__image_view_logo);
    }

    @Override
    public void setupView() {

        Animation fadeAnimation = AnimationUtils.loadAnimation(this, R.anim.anim_fade);

        mLogoImageView.startAnimation(fadeAnimation);

        fadeAnimation.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                User user = ServappPreferenceManager.getUser(SplashActivity.this);
                Intent intent = new Intent(SplashActivity.this, user == null ? UserActivity.class : MainActivity.class);

                if (user != null) {
                    intent.putExtra(KEY_USER, user);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                }

                startActivity(intent);

                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }
}