package com.core.rezaee.servapp.profile;

import com.core.rezaee.servapp.models.requests.ProfileSaveRequestModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ProfileApi {

    @POST("profiles/save")
    Call<Void> save(@Body ProfileSaveRequestModel model);
}