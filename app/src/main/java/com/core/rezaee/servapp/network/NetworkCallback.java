package com.core.rezaee.servapp.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.core.rezaee.servapp.Helper;
import com.core.rezaee.servapp.R;
import com.core.rezaee.servapp.customviews.ProgressDialogFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.core.rezaee.servapp.Constant.TAG_PROGRESS_DIALOG;

public class NetworkCallback<T> implements Callback<T> {

    private Context mContext;
    private boolean mShowProgressDialog;
    private ProgressDialogFragment mProgressDialog;

    public NetworkCallback(Context context) {

        this(context, false);
    }

    public NetworkCallback(Context context, boolean showProgressDialog) {

        mContext = context;
        mShowProgressDialog = showProgressDialog;

        Helper.hideKeypad((FragmentActivity) context);

        if (mShowProgressDialog) {
            showProgressDialog();
        }
    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {

        if (mShowProgressDialog) {
            dismissProgressDialog();
        }
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable e) {

        if (mShowProgressDialog) {
            dismissProgressDialog();
        }
    }

    private void showProgressDialog() {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialogFragment().setMessage(mContext.getString(R.string.app__please_wait));
        }

        try {
            mProgressDialog.show(((FragmentActivity) mContext).getSupportFragmentManager(), TAG_PROGRESS_DIALOG);
        } catch (Exception ignored) {
        }
    }

    private void dismissProgressDialog() {

        if (mProgressDialog != null) {
            try {
                mProgressDialog.dismiss();
            } catch (Exception ignored) {
            }
        }
    }
}