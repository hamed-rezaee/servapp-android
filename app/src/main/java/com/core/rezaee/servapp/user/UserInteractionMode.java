package com.core.rezaee.servapp.user;

public enum UserInteractionMode {

    LOGIN,
    REGISTER,
    ACTIVATION_CODE,
    FORGOT_ACCOUNT,
    VERIFY
}