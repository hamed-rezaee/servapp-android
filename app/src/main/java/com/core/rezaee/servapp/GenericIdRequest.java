package com.core.rezaee.servapp;

import com.core.rezaee.servapp.models.requests.GenericIdRequestModel;

public class GenericIdRequest {

    public String id;

    public GenericIdRequest(String id) {

        this.id = id;
    }

    public GenericIdRequestModel getModel() {

        GenericIdRequestModel model = new GenericIdRequestModel();

        model._id = id;

        return model;
    }
}