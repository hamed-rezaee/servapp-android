package com.core.rezaee.servapp;

public class Constant {

    public static final String KEY_PACKAGE_NAME = "com.core.rezaee.servapp";

    public static final String KEY_MAP_CLIENT_ID = "servapp-4491909206771554308";
    public static final String KEY_MAP_CLIENT_SECRET = "b0y-9XNlcnZhcHArfuHoll8bGhzBatcjcoHehk2r8S984zOT9eYy9s5KHg==";

    public static final String KEY_BASE_URL = "http://10.0.2.2:3000/api/v1/";
    public static final String KEY_HEADER_TOKEN_NAME = "x-jwt-token";

    public static final String[] SMS_PROVIDER_NUMBERS = {"6505551212"};

    public static final String KEY_PERSIAN = "fa";
    public static final String KEY_ENGLISH = "en";

    public static final String KEY_PREFERENCES_LANGUAGE = "preferences_language";

    public static final String FONT_FA_PATH = "fonts/iran_sans_mobile_light.ttf";

    public static final int MIN_PHONE_NO_LENGTH = 11;
    public static final int MIN_TEXT_CHARACTER_LENGTH = 8;
    public static final int MIN_PASSWORD_CHARACTER_LENGTH = 8;

    public static final int DELAY_ONE_SECOND = 1000;
    public static final int VERIFY_USER_TIMEOUT_SECOND = 60000;

    public static final String TAG_PROGRESS_DIALOG = "tag_progress_dialog";
    public static final String TAG_CONFIRM_DIALOG = "tag_confirm_dialog";
    public static final String TAG_ALERT_DIALOG = "tag_alert_dialog";

    public static final String EMPTY_STRING = "";
    public static final String FULL_DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";
    public static final String DATE_FORMAT = "yyyy/MM/dd";
    public static final String TIME_FORMAT = "HH:mm";
}