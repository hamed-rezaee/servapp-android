package com.core.rezaee.servapp.address;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import com.core.rezaee.servapp.BaseActivity;
import com.core.rezaee.servapp.Constant;
import com.core.rezaee.servapp.R;

import static com.core.rezaee.servapp.MainActivity.KEY_USER_ID;

public class AddressActivity extends BaseActivity {

    private static final String TAG_CURRENT_FRAGMENT = "tag_current_fragment";

    public static final String KEY_PROFILE = "key_profile";

    private Toolbar mMainToolbar;
    private Fragment mCurrentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_address);

        if (savedInstanceState == null) {

            String userId = Constant.EMPTY_STRING;

            if (getIntent().getExtras() != null) {
                userId = getIntent().getExtras().getString(KEY_USER_ID);
            }

            setFragment(userId);
        }

        initializeView();
        setupView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

        getSupportFragmentManager().putFragment(outState, TAG_CURRENT_FRAGMENT, mCurrentFragment);
    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();

        return true;
    }

    @Override
    public void initializeView() {

        mMainToolbar = findViewById(R.id.toolbar_main);
    }

    @Override
    public void setupView() {

        setSupportActionBar(mMainToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        setTitle(R.string.app__addresses);
    }

    public void onRestoreInstanceState(Bundle inState) {

        if (inState != null) {
            mCurrentFragment = getSupportFragmentManager().getFragment(inState, TAG_CURRENT_FRAGMENT);
        }
    }

    public void setFragment(String userId) {

        mCurrentFragment = AddressFragment.newInstance(userId);

        getSupportFragmentManager().beginTransaction().replace(R.id.activity_address__view_container, mCurrentFragment, TAG_CURRENT_FRAGMENT).commit();
    }
}