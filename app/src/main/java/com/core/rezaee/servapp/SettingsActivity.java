package com.core.rezaee.servapp;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class SettingsActivity extends BaseActivity {

    public static final String TAG_SETTINGS_FRAGMENT = "tag_settings_fragment";

    private static final String KEY_NEEDS_TO_RELOAD = "key_needs_to_reload";

    public boolean needsToReload;

    private Toolbar mMainToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        if (savedInstanceState != null) {
            needsToReload = savedInstanceState.getBoolean(KEY_NEEDS_TO_RELOAD, false);
        }

        initializeView();
        setupView();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

        outState.putBoolean(KEY_NEEDS_TO_RELOAD, needsToReload);
    }

    @Override
    public void onBackPressed() {

        setResult(needsToReload ? RESULT_OK : RESULT_CANCELED);

        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();

        return true;
    }

    @Override
    public void initializeView() {

        mMainToolbar = findViewById(R.id.toolbar_main);
    }

    @Override
    public void setupView() {

        setSupportActionBar(mMainToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        setTitle(R.string.app__settings);

        getFragmentManager().beginTransaction().replace(R.id.activity_settings__view_main_container, new SettingsFragment(), TAG_SETTINGS_FRAGMENT).commit();
    }
}