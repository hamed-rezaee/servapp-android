package com.core.rezaee.servapp.profile;

import android.content.Context;
import android.support.annotation.NonNull;

import com.core.rezaee.servapp.Helper;
import com.core.rezaee.servapp.ServerError;
import com.core.rezaee.servapp.address.Address;
import com.core.rezaee.servapp.file.File;
import com.core.rezaee.servapp.models.ProfileModel;
import com.core.rezaee.servapp.network.NetworkCallback;
import com.core.rezaee.servapp.network.NetworkHelper;
import com.core.rezaee.servapp.user.User;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class Profile implements Serializable {

    private static final String LOGGER_TAG = "Profile";

    public String id;
    public String firstName;
    public String lastName;
    public String nationalCode;
    public int score;

    public User user;
    public File avatar;
    public List<Address> addresses;

    public Profile(ProfileModel model) {

        if (model == null) {
            return;
        }

        id = model._id;
        firstName = model.firstName;
        lastName = model.lastName;
        nationalCode = model.nationalCode;
        score = model.score;

        user = new User(model.user);
        avatar = new File(model.avatar);
        addresses = Helper.exchange(model.addresses);
    }

    public ProfileModel getModel() {

        ProfileModel model = new ProfileModel();

        model._id = id;
        model.firstName = firstName;
        model.lastName = lastName;
        model.nationalCode = nationalCode;
        model.score = score;

        model.user = user.getModel();
        model.avatar = avatar.getModel();
        model.addresses = Helper.exchange(addresses);

        return model;
    }

    public static void save(Context context, final int requestCode, final ProfileCallback callback, ProfileSaveRequest request) {

        ProfileApi profileApi = NetworkHelper.getInstance(context).create(ProfileApi.class);
        Call<Void> call = profileApi.save(request.getModel());

        call.enqueue(new NetworkCallback<Void>(context, true) {

            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {

                super.onResponse(call, response);

                if (!response.isSuccessful()) {
                    callback.onException(requestCode, ServerError.getError(response.errorBody()));
                } else {
                    callback.onProfileSaved(requestCode);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable e) {

                super.onFailure(call, e);

                callback.onException(requestCode, ServerError.getError(null));
            }
        });
    }
}