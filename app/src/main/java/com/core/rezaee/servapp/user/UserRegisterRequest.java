package com.core.rezaee.servapp.user;

import com.core.rezaee.servapp.models.requests.UserRegisterRequestModel;

public class UserRegisterRequest {

    public String username;
    public String phoneNo;
    public String password;
    public String presenterCode;

    public UserRegisterRequestModel getModel() {

        UserRegisterRequestModel model = new UserRegisterRequestModel();

        model.username = username;
        model.phoneNo = phoneNo;
        model.password = password;
        model.presenterCode = presenterCode;

        return model;
    }
}