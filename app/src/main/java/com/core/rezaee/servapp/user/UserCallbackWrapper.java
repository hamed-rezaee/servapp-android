package com.core.rezaee.servapp.user;

import com.core.rezaee.servapp.ServerError;

public class UserCallbackWrapper implements UserCallback {

    @Override
    public void onException(int requestCode, ServerError serverError) {
    }

    @Override
    public void onUserRegistered(int requestCode, User user) {
    }

    @Override
    public void onTokenFetched(int requestCode, User user) {
    }

    @Override
    public void onActivationCodeFetched(int requestCode) {
    }

    @Override
    public void onPhoneNoValidated(int requestCode) {
    }

    @Override
    public void onUserFetched(int requestCode, User user) {
    }

    @Override
    public void onAccountInformationSent(int requestCode) {
    }
}