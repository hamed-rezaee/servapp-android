package com.core.rezaee.servapp;

import com.core.rezaee.servapp.models.ServerErrorModel;
import com.core.rezaee.servapp.network.Error;
import com.google.gson.Gson;

import okhttp3.ResponseBody;

import static com.core.rezaee.servapp.network.Error.ERROR_UNKNOWN_ERROR;

public class ServerError {

    public Error code;
    public String message;

    public ServerError(ServerErrorModel model) {

        if (model == null) {
            code = ERROR_UNKNOWN_ERROR;
            message = Constant.EMPTY_STRING;
        } else {
            code = Error.getServerError(model.code);
            message = model.message == null ? Constant.EMPTY_STRING : model.message;
        }
    }

    public static ServerError getError(ResponseBody errorBody) {

        ServerErrorModel errorModel = null;

        try {
            errorModel = new Gson().fromJson(errorBody.string(), ServerErrorModel.class);
        } catch (Exception ignored) {
        }

        return new ServerError(errorModel);
    }
}