package com.core.rezaee.servapp.customviews;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

import com.core.rezaee.servapp.Helper;

public class CustomTextInputLayout extends TextInputLayout {

    public CustomTextInputLayout(Context context) {

        super(context);

        init();
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs) {

        super(context, attrs);

        init();
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {

        super(context, attrs, defStyleAttr);

        init();
    }

    private void init() {

        setTypeface(Helper.getTypeface(getContext()));
    }
}