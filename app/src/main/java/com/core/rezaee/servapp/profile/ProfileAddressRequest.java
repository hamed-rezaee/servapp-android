package com.core.rezaee.servapp.profile;

import com.core.rezaee.servapp.models.requests.ProfileAddressRequestModel;

public class ProfileAddressRequest {

    public String profileId;

    public ProfileAddressRequest(String profileId) {

        this.profileId = profileId;
    }

    public ProfileAddressRequestModel getModel() {

        ProfileAddressRequestModel model = new ProfileAddressRequestModel();

        model.profileId = profileId;

        return model;
    }
}