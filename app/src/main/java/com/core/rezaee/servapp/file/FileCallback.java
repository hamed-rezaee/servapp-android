package com.core.rezaee.servapp.file;

import com.core.rezaee.servapp.network.NetworkErrorCallback;

public interface FileCallback extends NetworkErrorCallback {

    void onFileDownloaded(int requestCode, File file);

    void onFileUploaded(int requestCode, File file);
}